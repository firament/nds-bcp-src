﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("USER_PROFILES")]
    public partial class UserProfiles
    {
        [Key]
        [Column("USER_ID", TypeName = "int(11)")]
        public int UserId { get; set; }
        [Required]
        [Column("FULL_NAME")]
        [StringLength(60)]
        public string FullName { get; set; }
        [Column("LICENSE_NUM")]
        [StringLength(60)]
        public string LicenseNum { get; set; }
        [Column("GST_NUM")]
        [StringLength(24)]
        public string GstNum { get; set; }
        [Column("PAN_NUM")]
        [StringLength(24)]
        public string PanNum { get; set; }
        [Column("LANGUAGES_SPOKEN")]
        [StringLength(60)]
        public string LanguagesSpoken { get; set; }
        [Column("OPERATING_DISTRICTS")]
        [StringLength(60)]
        public string OperatingDistricts { get; set; }
        [Column("OPERATING_STATES")]
        [StringLength(60)]
        public string OperatingStates { get; set; }
        [Column("OPERATING_YEARS", TypeName = "int(11)")]
        public int OperatingYears { get; set; }
        [Column("DRIVING_LIC_NUM")]
        [StringLength(24)]
        public string DrivingLicNum { get; set; }
        [Column("DRIVING_LIC_EXPIRY", TypeName = "date")]
        public DateTime? DrivingLicExpiry { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("UserId")]
        [InverseProperty("UserProfiles")]
        public virtual Users User { get; set; }
    }
}
