﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace bcp.Data.dbsets
{
    public partial class BCP_DBContext : DbContext
    {
        public BCP_DBContext()
        {
        }

        public BCP_DBContext(DbContextOptions<BCP_DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ADbInfo> ADbInfo { get; set; }
        public virtual DbSet<DdCategories> DdCategories { get; set; }
        public virtual DbSet<DdItems> DdItems { get; set; }
        public virtual DbSet<GenAppSettings> GenAppSettings { get; set; }
        public virtual DbSet<GenLocations> GenLocations { get; set; }
        public virtual DbSet<LegalDocuments> LegalDocuments { get; set; }
        public virtual DbSet<LineTrucks> LineTrucks { get; set; }
        public virtual DbSet<MaintLoginHistory> MaintLoginHistory { get; set; }
        public virtual DbSet<MaintSessions> MaintSessions { get; set; }
        public virtual DbSet<MaintUserVerify> MaintUserVerify { get; set; }
        public virtual DbSet<QuoteRequests> QuoteRequests { get; set; }
        public virtual DbSet<QuoteResponseIncludes> QuoteResponseIncludes { get; set; }
        public virtual DbSet<QuoteResponses> QuoteResponses { get; set; }
        public virtual DbSet<SystemLogBcp> SystemLogBcp { get; set; }
        public virtual DbSet<Trucks> Trucks { get; set; }
        public virtual DbSet<UserAddresses> UserAddresses { get; set; }
        public virtual DbSet<UserDocumentMap> UserDocumentMap { get; set; }
        public virtual DbSet<UserProfiles> UserProfiles { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<ADbInfo>(entity =>
            {
                entity.HasIndex(e => new { e.AppCode, e.Version })
                    .HasName("UNIQ_VERSIONS")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AppCode).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Version).IsUnicode(false);
            });

            modelBuilder.Entity<DdCategories>(entity =>
            {
                entity.HasIndex(e => e.DdiCatgName)
                    .HasName("DDI_CATG_NAME")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DdiCatgDesc).IsUnicode(false);

                entity.Property(e => e.DdiCatgName).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<DdItems>(entity =>
            {
                entity.HasKey(e => new { e.DdiCatgId, e.DdiCode });

                entity.HasIndex(e => new { e.DdiCatgId, e.Status })
                    .HasName("DD_ITEMS");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DdiDesc).IsUnicode(false);

                entity.Property(e => e.DdiDispSeq).HasDefaultValueSql("10000");

                entity.Property(e => e.DdiDispText).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.DdiCatg)
                    .WithMany(p => p.DdItems)
                    .HasForeignKey(d => d.DdiCatgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKDD_ITEMS184240");
            });

            modelBuilder.Entity<GenAppSettings>(entity =>
            {
                entity.HasIndex(e => new { e.AppCode, e.AppConfigVer })
                    .HasName("GEN_APP_SETTINGS");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AppCode).IsUnicode(false);

                entity.Property(e => e.AppConfig).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<GenLocations>(entity =>
            {
                entity.HasIndex(e => e.LocFull)
                    .HasName("LOC_FULL")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.Country)
                    .IsUnicode(false)
                    .HasDefaultValueSql("\"INDIA\"");

                entity.Property(e => e.CountryCode)
                    .IsUnicode(false)
                    .HasDefaultValueSql("\"IN\"");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.IsCity).HasDefaultValueSql("0");

                entity.Property(e => e.IsCustom).HasDefaultValueSql("0");

                entity.Property(e => e.LocFull).IsUnicode(false);

                entity.Property(e => e.LocLat).HasDefaultValueSql("0.00000000");

                entity.Property(e => e.LocLon).HasDefaultValueSql("0.00000000");

                entity.Property(e => e.PinCodes).IsUnicode(false);

                entity.Property(e => e.State).IsUnicode(false);

                entity.Property(e => e.StateCode).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Town).IsUnicode(false);
            });

            modelBuilder.Entity<LegalDocuments>(entity =>
            {
                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DocHash).IsUnicode(false);

                entity.Property(e => e.DocNumber).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FileNameSys).IsUnicode(false);

                entity.Property(e => e.FileNameUser).IsUnicode(false);

                entity.Property(e => e.MimeType).HasDefaultValueSql("0");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Type).HasDefaultValueSql("0");

                entity.Property(e => e.Version).IsUnicode(false);
            });

            modelBuilder.Entity<LineTrucks>(entity =>
            {
                entity.HasIndex(e => e.OperatedBy)
                    .HasName("FKLINE_TRUCK337802");

                entity.HasIndex(e => e.QuoteResponseId)
                    .HasName("FKLINE_TRUCK465262");

                entity.HasIndex(e => e.RegnNum)
                    .HasName("REGN_NUM")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.IsOwn).HasDefaultValueSql("0");

                entity.Property(e => e.Make).IsUnicode(false);

                entity.Property(e => e.Model).IsUnicode(false);

                entity.Property(e => e.OwnerContact).IsUnicode(false);

                entity.Property(e => e.OwnerContactAlt).IsUnicode(false);

                entity.Property(e => e.RcCardFace1).IsUnicode(false);

                entity.Property(e => e.RcCardFace2).IsUnicode(false);

                entity.Property(e => e.RegdOwner).IsUnicode(false);

                entity.Property(e => e.RegnNum).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Type).IsUnicode(false);

                entity.HasOne(d => d.OperatedByNavigation)
                    .WithMany(p => p.LineTrucks)
                    .HasForeignKey(d => d.OperatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKLINE_TRUCK337802");

                entity.HasOne(d => d.QuoteResponse)
                    .WithMany(p => p.LineTrucks)
                    .HasForeignKey(d => d.QuoteResponseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKLINE_TRUCK465262");
            });

            modelBuilder.Entity<MaintLoginHistory>(entity =>
            {
                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.UserLogin).IsUnicode(false);
            });

            modelBuilder.Entity<MaintSessions>(entity =>
            {
                entity.HasIndex(e => e.Token)
                    .HasName("MAINT_SESSIONS");

                entity.Property(e => e.AuthTime).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeviceSig).IsUnicode(false);

                entity.Property(e => e.IssueTime).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LastUsed).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PayloadData).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Token).IsUnicode(false);

                entity.Property(e => e.UserLogin).IsUnicode(false);

                entity.Property(e => e.UserName).IsUnicode(false);
            });

            modelBuilder.Entity<MaintUserVerify>(entity =>
            {
                entity.HasIndex(e => e.Mobile)
                    .HasName("MOBILE");

                entity.HasIndex(e => e.Status)
                    .HasName("STATUS");

                entity.HasIndex(e => e.VerifyPin)
                    .HasName("VERIFY_PIN");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Payload).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<QuoteRequests>(entity =>
            {
                entity.HasIndex(e => e.ContractNum)
                    .HasName("CONTRACT_NUM")
                    .IsUnique();

                entity.HasIndex(e => e.QuoteOwner)
                    .HasName("FKQUOTE_REQU285861");

                entity.HasIndex(e => e.TransporterId)
                    .HasName("FKQUOTE_REQU638345");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AgreementId).IsUnicode(false);

                entity.Property(e => e.ClassOfGoods).HasDefaultValueSql("0");

                entity.Property(e => e.ContractNum).IsUnicode(false);

                entity.Property(e => e.Destination).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Insurance).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Origin).IsUnicode(false);

                entity.Property(e => e.PaymentTerms).IsUnicode(false);

                entity.Property(e => e.RespondBy).IsUnicode(false);

                entity.Property(e => e.SpecialConditions).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.TruckTonnage).HasDefaultValueSql("0");

                entity.Property(e => e.TruckType).HasDefaultValueSql("0");

                entity.HasOne(d => d.QuoteOwnerNavigation)
                    .WithMany(p => p.QuoteRequestsQuoteOwnerNavigation)
                    .HasForeignKey(d => d.QuoteOwner)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKQUOTE_REQU285861");

                entity.HasOne(d => d.Transporter)
                    .WithMany(p => p.QuoteRequestsTransporter)
                    .HasForeignKey(d => d.TransporterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKQUOTE_REQU638345");
            });

            modelBuilder.Entity<QuoteResponseIncludes>(entity =>
            {
                entity.HasKey(e => new { e.ResponseId, e.Inclusions });

                entity.Property(e => e.Inclusions).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.IsIncluded).HasDefaultValueSql("1");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.Response)
                    .WithMany(p => p.QuoteResponseIncludes)
                    .HasForeignKey(d => d.ResponseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKQUOTE_RESP474982");
            });

            modelBuilder.Entity<QuoteResponses>(entity =>
            {
                entity.HasIndex(e => e.ContractNum)
                    .HasName("FKQUOTE_RESP791117");

                entity.HasIndex(e => e.QuotedBy)
                    .HasName("FKQUOTE_RESP124198");

                entity.HasIndex(e => e.TruckId)
                    .HasName("FKQUOTE_RESP630367");

                entity.HasIndex(e => new { e.QuoteId, e.QuotedBy })
                    .HasName("QUOTE_RESPONSES")
                    .IsUnique();

                entity.HasIndex(e => new { e.ResponseId, e.QuoteId, e.QuotedBy })
                    .HasName("Unique_Response")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CapitalAssistAmt).HasDefaultValueSql("0.00");

                entity.Property(e => e.CapitalAssistReqd).HasDefaultValueSql("0");

                entity.Property(e => e.ContractNum).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LoadUom).HasDefaultValueSql("0");

                entity.Property(e => e.NotesDeviations).IsUnicode(false);

                entity.Property(e => e.RateUnitLoad).HasDefaultValueSql("0.00");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.ContractNumNavigation)
                    .WithMany(p => p.QuoteResponsesContractNumNavigation)
                    .HasPrincipalKey(p => p.ContractNum)
                    .HasForeignKey(d => d.ContractNum)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKQUOTE_RESP791117");

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteResponsesQuote)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKQUOTE_RESP814553");

                entity.HasOne(d => d.QuotedByNavigation)
                    .WithMany(p => p.QuoteResponses)
                    .HasForeignKey(d => d.QuotedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKQUOTE_RESP124198");

                entity.HasOne(d => d.Truck)
                    .WithMany(p => p.QuoteResponses)
                    .HasForeignKey(d => d.TruckId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKQUOTE_RESP630367");
            });

            modelBuilder.Entity<SystemLogBcp>(entity =>
            {
                entity.Property(e => e.ActivityId).IsUnicode(false);

                entity.Property(e => e.AllEventProps).IsUnicode(false);

                entity.Property(e => e.AppCode).IsUnicode(false);

                entity.Property(e => e.CallSite).IsUnicode(false);

                entity.Property(e => e.Exception).IsUnicode(false);

                entity.Property(e => e.LogLevel).IsUnicode(false);

                entity.Property(e => e.LogTime).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Logger).IsUnicode(false);

                entity.Property(e => e.Machine).IsUnicode(false);

                entity.Property(e => e.Message).IsUnicode(false);

                entity.Property(e => e.Stacktrace).IsUnicode(false);
            });

            modelBuilder.Entity<Trucks>(entity =>
            {
                entity.HasIndex(e => e.OperatedBy)
                    .HasName("FKTRUCKS792159");

                entity.HasIndex(e => e.RegnNum)
                    .HasName("REGN_NUM")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.IsOwn).HasDefaultValueSql("0");

                entity.Property(e => e.MakeModel).IsUnicode(false);

                entity.Property(e => e.Model).HasDefaultValueSql("0");

                entity.Property(e => e.OwnerContact).IsUnicode(false);

                entity.Property(e => e.OwnerContactAlt).IsUnicode(false);

                entity.Property(e => e.RcCardFace1).IsUnicode(false);

                entity.Property(e => e.RcCardFace2).IsUnicode(false);

                entity.Property(e => e.RegdOwner).IsUnicode(false);

                entity.Property(e => e.RegnNum).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Tonnage).HasDefaultValueSql("0");

                entity.Property(e => e.Type).HasDefaultValueSql("0");

                entity.HasOne(d => d.OperatedByNavigation)
                    .WithMany(p => p.Trucks)
                    .HasForeignKey(d => d.OperatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKTRUCKS792159");
            });

            modelBuilder.Entity<UserAddresses>(entity =>
            {
                entity.HasKey(e => new { e.AddrsId, e.UserId, e.AddrsSeq });

                entity.HasIndex(e => e.UserId)
                    .HasName("FKUSER_ADDRE747177");

                entity.Property(e => e.AddrsId).ValueGeneratedOnAdd();

                entity.Property(e => e.AddrsSeq).HasDefaultValueSql("10000");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AddrLine1).IsUnicode(false);

                entity.Property(e => e.AddrLine2).IsUnicode(false);

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.CityStateText).IsUnicode(false);

                entity.Property(e => e.ContactEmail).IsUnicode(false);

                entity.Property(e => e.ContactMobile).IsUnicode(false);

                entity.Property(e => e.ContactName).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Gst).IsUnicode(false);

                entity.Property(e => e.Pin).IsUnicode(false);

                entity.Property(e => e.State).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Tag)
                    .IsUnicode(false)
                    .HasDefaultValueSql("\"Default Address\"");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAddresses)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKUSER_ADDRE747177");
            });

            modelBuilder.Entity<UserDocumentMap>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.DocumentId });

                entity.HasIndex(e => e.DocumentId)
                    .HasName("FKUSER_DOCUM903980");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.UserDocumentMap)
                    .HasForeignKey(d => d.DocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKUSER_DOCUM903980");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserDocumentMap)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKUSER_DOCUM945036");
            });

            modelBuilder.Entity<UserProfiles>(entity =>
            {
                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DrivingLicNum).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FullName).IsUnicode(false);

                entity.Property(e => e.GstNum).IsUnicode(false);

                entity.Property(e => e.LanguagesSpoken).IsUnicode(false);

                entity.Property(e => e.LicenseNum).IsUnicode(false);

                entity.Property(e => e.OperatingDistricts).IsUnicode(false);

                entity.Property(e => e.OperatingStates).IsUnicode(false);

                entity.Property(e => e.OperatingYears).HasDefaultValueSql("0");

                entity.Property(e => e.PanNum).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.UserProfiles)
                    .HasForeignKey<UserProfiles>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKUSER_PROFI365963");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasIndex(e => e.Login)
                    .HasName("USERS");

                entity.HasIndex(e => e.Mobile)
                    .HasName("MOBILE")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.FullName).IsUnicode(false);

                entity.Property(e => e.LastLogin).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Login).IsUnicode(false);

                entity.Property(e => e.Mobile).IsUnicode(false);

                entity.Property(e => e.Role).HasDefaultValueSql("400");

                entity.Property(e => e.ScreenName).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Type).HasDefaultValueSql("0");

                entity.Property(e => e.UserPwd).IsUnicode(false);
            });
        }
    }
}
