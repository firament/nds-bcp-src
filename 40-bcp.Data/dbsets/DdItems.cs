﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("DD_ITEMS")]
    public partial class DdItems
    {
        [Column("DDI_CATG_ID", TypeName = "int(11)")]
        public int DdiCatgId { get; set; }
        [Column("DDI_CODE", TypeName = "int(11)")]
        public int DdiCode { get; set; }
        [Column("DDI_DISP_SEQ", TypeName = "int(11)")]
        public int DdiDispSeq { get; set; }
        [Required]
        [Column("DDI_DISP_TEXT")]
        [StringLength(24)]
        public string DdiDispText { get; set; }
        [Required]
        [Column("DDI_DESC")]
        [StringLength(255)]
        public string DdiDesc { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("DdiCatgId")]
        [InverseProperty("DdItems")]
        public virtual DdCategories DdiCatg { get; set; }
    }
}
