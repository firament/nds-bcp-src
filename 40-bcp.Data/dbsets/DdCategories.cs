﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("DD_CATEGORIES")]
    public partial class DdCategories
    {
        public DdCategories()
        {
            DdItems = new HashSet<DdItems>();
        }

        [Key]
        [Column("DDI_CATG_ID", TypeName = "int(11)")]
        public int DdiCatgId { get; set; }
        [Required]
        [Column("DDI_CATG_NAME")]
        [StringLength(24)]
        public string DdiCatgName { get; set; }
        [Required]
        [Column("DDI_CATG_DESC")]
        [StringLength(255)]
        public string DdiCatgDesc { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [InverseProperty("DdiCatg")]
        public virtual ICollection<DdItems> DdItems { get; set; }
    }
}
