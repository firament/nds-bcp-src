﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("MAINT_LOGIN_HISTORY")]
    public partial class MaintLoginHistory
    {
        [Key]
        [Column("MLH_ID", TypeName = "int(11)")]
        public int MlhId { get; set; }
        [Column("USER_ID", TypeName = "int(11)")]
        public int UserId { get; set; }
        [Required]
        [Column("USER_LOGIN")]
        [StringLength(24)]
        public string UserLogin { get; set; }
        [Column("NOTE")]
        public string Note { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
    }
}
