﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("USER_DOCUMENT_MAP")]
    public partial class UserDocumentMap
    {
        [Column("USER_ID", TypeName = "int(11)")]
        public int UserId { get; set; }
        [Column("DOCUMENT_ID", TypeName = "int(11)")]
        public int DocumentId { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("DocumentId")]
        [InverseProperty("UserDocumentMap")]
        public virtual LegalDocuments Document { get; set; }
        [ForeignKey("UserId")]
        [InverseProperty("UserDocumentMap")]
        public virtual Users User { get; set; }
    }
}
