﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("MAINT_SESSIONS")]
    public partial class MaintSessions
    {
        [Key]
        [Column("MSID", TypeName = "bigint(20)")]
        public long Msid { get; set; }
        [Required]
        [Column("TOKEN")]
        [StringLength(255)]
        public string Token { get; set; }
        [Required]
        [Column("DEVICE_SIG")]
        public string DeviceSig { get; set; }
        [Column("USERID", TypeName = "int(11)")]
        public int Userid { get; set; }
        [Required]
        [Column("USER_LOGIN")]
        [StringLength(60)]
        public string UserLogin { get; set; }
        [Required]
        [Column("USER_NAME")]
        [StringLength(24)]
        public string UserName { get; set; }
        [Column("USER_TYPE", TypeName = "int(11)")]
        public int UserType { get; set; }
        [Column("USER_ROLE", TypeName = "int(11)")]
        public int UserRole { get; set; }
        [Column("ISSUE_TIME")]
        public DateTimeOffset IssueTime { get; set; }
        [Column("AUTH_TIME")]
        public DateTimeOffset AuthTime { get; set; }
        [Column("LAST_USED")]
        public DateTimeOffset LastUsed { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Required]
        [Column("PAYLOAD_DATA")]
        public string PayloadData { get; set; }
    }
}
