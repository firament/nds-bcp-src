﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("USER_ADDRESSES")]
    public partial class UserAddresses
    {
        [Column("ADDRS_ID", TypeName = "int(11)")]
        public int AddrsId { get; set; }
        [Column("USER_ID", TypeName = "int(11)")]
        public int UserId { get; set; }
        [Column("ADDRS_SEQ", TypeName = "int(11)")]
        public int AddrsSeq { get; set; }
        [Required]
        [Column("TAG")]
        [StringLength(24)]
        public string Tag { get; set; }
        [Required]
        [Column("CONTACT_NAME")]
        [StringLength(60)]
        public string ContactName { get; set; }
        [Required]
        [Column("CONTACT_MOBILE")]
        public string ContactMobile { get; set; }
        [Column("CONTACT_EMAIL")]
        [StringLength(40)]
        public string ContactEmail { get; set; }
        [Required]
        [Column("ADDR_LINE_1")]
        [StringLength(255)]
        public string AddrLine1 { get; set; }
        [Column("ADDR_LINE_2")]
        [StringLength(255)]
        public string AddrLine2 { get; set; }
        [Required]
        [Column("CITY")]
        [StringLength(24)]
        public string City { get; set; }
        [Required]
        [Column("STATE")]
        [StringLength(8)]
        public string State { get; set; }
        [Required]
        [Column("PIN")]
        [StringLength(8)]
        public string Pin { get; set; }
        [Required]
        [Column("CITY_STATE_TEXT")]
        [StringLength(60)]
        public string CityStateText { get; set; }
        [Column("GST")]
        [StringLength(24)]
        public string Gst { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("UserId")]
        [InverseProperty("UserAddresses")]
        public virtual Users User { get; set; }
    }
}
