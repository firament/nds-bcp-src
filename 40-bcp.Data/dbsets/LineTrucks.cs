﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("LINE_TRUCKS")]
    public partial class LineTrucks
    {
        public LineTrucks()
        {
            QuoteResponses = new HashSet<QuoteResponses>();
        }

        [Key]
        [Column("TRUCK_ID", TypeName = "int(11)")]
        public int TruckId { get; set; }
        [Column("QUOTE_RESPONSE_ID", TypeName = "int(11)")]
        public int QuoteResponseId { get; set; }
        [Column("OPERATED_BY", TypeName = "int(11)")]
        public int OperatedBy { get; set; }
        [Required]
        [Column("REGN_NUM")]
        [StringLength(20)]
        public string RegnNum { get; set; }
        [Required]
        [Column("MAKE")]
        [StringLength(24)]
        public string Make { get; set; }
        [Required]
        [Column("MODEL")]
        [StringLength(24)]
        public string Model { get; set; }
        [Column("TONNAGE", TypeName = "int(4)")]
        public int Tonnage { get; set; }
        [Required]
        [Column("TYPE")]
        [StringLength(24)]
        public string Type { get; set; }
        [Column("IS_OWN", TypeName = "int(11)")]
        public int IsOwn { get; set; }
        [Required]
        [Column("REGD_OWNER")]
        [StringLength(60)]
        public string RegdOwner { get; set; }
        [Required]
        [Column("OWNER_CONTACT")]
        [StringLength(16)]
        public string OwnerContact { get; set; }
        [Column("OWNER_CONTACT_ALT")]
        [StringLength(16)]
        public string OwnerContactAlt { get; set; }
        [Required]
        [Column("RC_CARD_FACE_1")]
        [StringLength(1020)]
        public string RcCardFace1 { get; set; }
        [Required]
        [Column("RC_CARD_FACE_2")]
        [StringLength(1020)]
        public string RcCardFace2 { get; set; }
        [Column("NOTE", TypeName = "int(11)")]
        public int? Note { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("OperatedBy")]
        [InverseProperty("LineTrucks")]
        public virtual Users OperatedByNavigation { get; set; }
        [ForeignKey("QuoteResponseId")]
        [InverseProperty("LineTrucks")]
        public virtual QuoteResponses QuoteResponse { get; set; }
        [InverseProperty("Truck")]
        public virtual ICollection<QuoteResponses> QuoteResponses { get; set; }
    }
}
