﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("QUOTE_RESPONSE_INCLUDES")]
    public partial class QuoteResponseIncludes
    {
        [Column("RESPONSE_ID", TypeName = "int(11)")]
        public int ResponseId { get; set; }
        [Column("INCLUSIONS", TypeName = "int(11)")]
        public int Inclusions { get; set; }
        [Column("IS_INCLUDED", TypeName = "int(11)")]
        public int IsIncluded { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("ResponseId")]
        [InverseProperty("QuoteResponseIncludes")]
        public virtual QuoteResponses Response { get; set; }
    }
}
