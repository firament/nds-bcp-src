#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
// using MySql.Data.EntityFrameworkCore;


// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;
#endregion // Using Declarations

namespace bcp.Data.Functions
{
	public static class CommonFunctions
	{
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		// /// <summary>
		// /// Convenience method to get Read-Only Connections
		// /// or connections to a seperate database instance
		// /// Use with caution.
		// /// TODO: 
		// /// 	1. Test connection string before using, or ake parameters and build one.
		// /// 	2. Should user close the connection, or just release it?
		// /// 	   a using clause should be sufficient.
		// /// </summary>
		// /// <param name="ConnectionString">Connection string to use with the database.</param>
		// /// <returns>An open connection, or null.</returns>
		// public static CTX.GPM_DBContext GetConnection(string ConnectionString)
		// {
		// 	CTX.GPM_DBContext New_DB = null;
		// 	try
		// 	{
		// 		New_DB = new CTX.GPM_DBContext(ConnectionString);
		// 	}
		// 	catch (System.Exception Ex)
		// 	{
		// 		log.Error(Ex, "Unable to open connection with the given connection string");
		// 		throw;
		// 	}
		// 	return New_DB;
		// }

		public static CC.IAppsettings GetAppSettings(string psConnString, string psApplicationCode, int piASVersion)
		{
			CC.IAppsettings lASC = new CC.Appsettings();

			try
			{
				string lsJSON = string.Empty;
				using (CTX efc_DB = new CTX(psConnString))
				{
					DM.GenAppSettings vAS = efc_DB.GenAppSettings
								.AsNoTracking()
								.Where(p =>
									   p.AppCode == psApplicationCode
									&& p.AppConfigVer == piASVersion
									&& p.Status == 1
									)
								.FirstOrDefault()
								;

					if (vAS == null)
					{
						log.Debug("GEN_APP_SETTINGS entry == null");
						// log.Info("Use sample to build settings: {0}", getSampleAppSettings());
						log.Error(new IndexOutOfRangeException(""), "Application Settings ver {0} Not Found, see EXCEPTION details", piASVersion);
						return lASC;
					}

					// extract json
					lsJSON = vAS.AppConfig;
					if (string.IsNullOrWhiteSpace(lsJSON))
					{
						log.Error(new ArgumentNullException("AS_DATA", "Cannot deserialize from empty string"), "Data string is empty");
						return lASC;
					}

					// convert to object
					lASC = CU.FromJSON(lsJSON, lASC.GetType()) as CC.IAppsettings;
					if (lASC == null)
					{
						log.Debug("lASC == null");
						return lASC;
					}
					if (lASC.AppConfigVer != vAS.AppConfigVer)
					{
						// This is NOT good... inspect
						log.Warn(
								new ArgumentOutOfRangeException(
									"AppVersion"
									, "GEN_APP_SETTINGS.AppConfigVer is not in sync with AppSettingContainer.AppVersion"
									)
							, "Proceeding for now, FIX THIS at the earliest"
							);
						lASC.AppConfigVer = vAS.AppConfigVer;  // needed to save version back
					}
					lASC.LastLoaded = System.DateTime.UtcNow;
				};
			}
			catch (System.Exception eX)
			{
				log.Error(eX, "Error getting ApplicatonSettings from database");
			}

			return lASC;
			// return lASC ?? GetSampleSettings();

		}

		// ADD FUNCTION TO INSERT A SAMPLE SETTING IN DB
		public static CC.IAppsettings GetSampleSettings(string psApplicationCode = "SAMPLE", int piASVersion = 0)
		{
			CC.IAppsettings loSample = new CC.Appsettings
			{
				AppCode = psApplicationCode,
				AppConfigVer = piASVersion,
				UploadLocation = "x/y/z",
				DisplayOpts = new CC.DisplayOptions
				{
					DateTimeFixed = "hh:mm tt on dd MMM yyyy",
					LatLon = "00.0000"
				}
			};
			return loSample;
		}

		public static void AddSampleSettings(string psConnString, string psApplicationCode, int piASVersion)
		{
			CC.IAppsettings lASC = GetSampleSettings(psApplicationCode, piASVersion);
			Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<DM.GenAppSettings> SaveResult = null;
			int liRes = 0;

			try
			{
				string lsJSON = string.Empty;
				using (CTX efc_DB = new CTX(psConnString))
				{
					SaveResult = efc_DB.GenAppSettings.Add(new DM.GenAppSettings
					{
						AppConfig = CU.ToJSON(lASC, false),
						AppCode = psApplicationCode,
						AppConfigVer = piASVersion,
					});
					liRes = efc_DB.SaveChanges();
					log.Info("Saved sample app settings. {0} records affected", liRes);
				};

			}
			catch (System.Exception eX)
			{
				log.Error(eX, "Error saving sample ApplicatonSettings to database");
			}
		}


	}
}
