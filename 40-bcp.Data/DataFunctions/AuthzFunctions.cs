#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;

// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x
#endregion // Using Declarations

namespace bcp.Data.Functions
{
	public class AuthzFunctions : BCP_DATA_BASE
	{
		public AuthzFunctions(ILogger<AuthzFunctions> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc, bcp.Data.dbsets.BCP_DBContext _ctx)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc, _ctx)
		{
		}

		public async Task<DM.MaintSessions> WriteSessionLog(DM.MaintSessions slog)
		{
			int liRes = 0;
			try
			{
				await CTX.MaintSessions.AddAsync(slog);
				liRes = await CTX.SaveChangesAsync();
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error writing main session entry");
			}
			return slog;
		}

		public async Task<DM.MaintSessions> GetSessionByToken(string token)
		{
			DM.MaintSessions loMS = null;
			if (string.IsNullOrWhiteSpace(token))
			{
				return loMS;
			}
			try
			{
				loMS = await CTX.MaintSessions.AsNoTracking().Where(y => y.Token == token).FirstOrDefaultAsync();
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error fetching main session using token.");
			}
			return loMS;
		}

		public async Task<DM.MaintSessions> GetCurrentSession()
		{
			DM.MaintSessions loMS = null;
			string lsToken = UPL?.Token;

			if (string.IsNullOrWhiteSpace(lsToken))
			{
				return loMS;
			}
			try
			{
				loMS = await CTX.MaintSessions
								// .AsNoTracking()
								.Where(y => y.Token == lsToken)
								.FirstOrDefaultAsync()
								;
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error fetching main session using token.");
			}
			return loMS;
		}

		public async Task<int> AddLoginHistoryEntry(DM.MaintLoginHistory entry)
		{
			int liRes = 0;
			try
			{
				entry.MlhId = 0;
				await CTX.MaintLoginHistory.AddAsync(entry);
				liRes = await CTX.SaveChangesAsync();
				log.LogInformation("MaintLoginHistory add entry successful. EntryID = {0}", entry.MlhId);
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error writing entry to MaintLoginHistory.");
			}
			return liRes;
		}

		public async Task<DM.Users> ValidateUserCreds(string UserLogin, string UserPassword)
		{
			log.LogDebug("Debugger hook");
			DM.Users loUser = null;
			int liRes = 0;
			try
			{
				liRes = await CTX.Users
									.Where(y => y.Login == UserLogin)
									.CountAsync()
									;
				if (liRes == 0)
				{
					log.LogInformation("User login {0} not found.", UserLogin);
				}
				else if (liRes > 1)
				{
					log.LogWarning("User Login {0}. Expected one record, but found {1} records. Failing login attempt.", UserLogin, liRes);
				}
				loUser = await CTX.Users
									.AsNoTracking()
									.Where(y => y.Login == UserLogin && y.UserPwd == UserPassword)
									.Include(z => z.UserProfiles)
									.FirstOrDefaultAsync()
									;
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error fetching details of user {0} from database", UserLogin);
			}
			return loUser;
		}

		public async Task<int> UpdateSessionLog(bool isNewLogin = false)
		{
			// Write audit logs
			DM.MaintSessions loSession;// = await DAF.GetCurrentSession();
			log.LogDebug("Updating session for ID = {0}, Token = {1}", UPL.Msid, UPL.Token);
			loSession = await CTX.MaintSessions
				.Where(y => y.Token == UPL.Token)
				.FirstOrDefaultAsync()
				;

			if (loSession == null)
			{
				log.LogError("Failed to get session entry for ID = {0}, Token = {1}. Creating New entry.", UPL.Msid, UPL.Token);
				// return CAEL.DB_NO_RESULT;
				// return 0;
				loSession = new DM.MaintSessions(UPL);
				CTX.MaintSessions.Add(loSession);
			}

			DateTime ldtNow = DateTime.UtcNow;
			loSession.FromUPL(UPL);
			// loSession.Token = UPL.Token;
			// loSession.Userid = UPL.Userid;
			// loSession.UserLogin = UPL.Login;
			// loSession.UserName = UPL.DisplayName;
			// loSession.UserType = (int)UPL.Type;
			// loSession.UserRole = (int)UPL.Role;
			// loSession.LastUsed = UPL.LastUsed = ldtNow;
			// loSession.User_Payload = UPL;
			// loSession.Status = (int)CDE.SESSION_STATUS.ACTIVE;

			if (isNewLogin)
			{
				log.LogInformation("Adding Login History for UserID = {0}, Login = {1}", UPL.Userid, UPL.Login);
				CTX.MaintLoginHistory.Add(new DM.MaintLoginHistory
				{
					UserId = UPL.Userid,
					UserLogin = UPL.Login,
					Status = (int)UPL.Status,
					AddOn = ldtNow,
				});
				// Updated last use tag of user
				if (UPL.Userid != APPCFG.SessionOpts.ANON_USER_ID)
				{
					DM.Users loUser = await CTX.Users.Where(y => y.UserId == UPL.Userid).FirstOrDefaultAsync();
					log.LogInformation("Updating Users.LastLogin for UserId = {0}, Login = {1}", UPL.Userid, UPL.Login);
					if (loUser != null) { loUser.LastLogin = ldtNow; }
				}
			}

			int liRes = 0;
			try
			{
				liRes = await CTX.SaveChangesAsync();
				log.LogInformation("Updated session for ID = {0}, Login = {1}, Token = {2}", UPL.Msid, UPL.Login, UPL.Token);
				if (isNewLogin) { UPL.Msid = loSession.Msid; }
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error fetching main session using token.");
			}

			return liRes;
		}

		public async Task<DM.UserProfiles> GetUserProfile(int id = 0)
		{
			DM.UserProfiles loProf = null;
			try
			{
				loProf = await CTX.UserProfiles
									.AsNoTracking()
									.Where(y => y.UserId == id)
									// .Include(y => y.User)
									// .Include(y => y.User.UserAddresses)
									.FirstOrDefaultAsync()
									;
				if (loProf == null)
				{
					loProf = new DM.UserProfiles
					{
						// TODO: Fill defaults
						UserId = id,
					};
				}
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error fetching user profile for user {0}.", id);
			}
			return loProf;
		}

		public async Task<int> SaveUserProfile(DM.UserProfiles profile)
		{
			return 0;
		}

	}
}
