#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x
#endregion // Using Declarations
using Microsoft.EntityFrameworkCore;

namespace bcp.Data.Functions
{
	public class QuotePOFunctions : BCP_DATA_BASE
	{
		internal DV.DD_Lookup DDHelp;
		public QuotePOFunctions(DV.DD_Lookup dd_help, ILogger<QuotePOFunctions> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc, bcp.Data.dbsets.BCP_DBContext _ctx)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc, _ctx)
		{
			DDHelp = dd_help;
		}


		/// <summary>
		/// Ignoring filter, get all for now
		/// </summary>
		/// <param name="forAnchorClient"></param>
		/// <returns></returns>
		public async Task<List<DM.Users>> GetTransporters(int forAnchorClient)
		{
			List<DM.Users> lstTransporters = null;
			try
			{
				lstTransporters = await CTX.Users
									.AsNoTracking()
									.Include(y => y.UserProfiles)
									.Where(y => y.Type == (int)CDE.USER_TYPE.TRANSPORTER || y.Type == (int)CDE.USER_TYPE.BOOKING_AGENT)
									.Where(y => y.Status == (int)CDE.USER_STATUS.ACTIVE)
									.OrderBy(y => y.UserProfiles.FullName)
									.ToListAsync()
									;
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Failed to get transporter list for Anchor client {0}", forAnchorClient);
			}
			return lstTransporters;
		}

		public async Task<DM.QuoteRequests> SaveQuote(DM.QuoteRequests quote)
		{
			DM.QuoteRequests loQuote = null;
			int liRes = 0;

			// Ideally this is where the audit details should be set
			try
			{
				CTX.QuoteRequests.Add(quote);
				liRes = await CTX.SaveChangesAsync();
				loQuote = quote;
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error saving quote request");
			}

			return loQuote;
		}

		public async Task<List<DM.QuoteRequests>> QuoteRequestList()
		{
			List<DM.QuoteRequests> lstRequests = new List<DM.QuoteRequests>();
			try
			{
				lstRequests = await CTX.QuoteRequests
									.AsNoTracking()
									.Include(y => y.QuoteOwnerNavigation)
									.Include(y => y.Transporter)
									.ToListAsync()
									;
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error fetching quote requests");
			}
			return lstRequests;
		}

		public async Task<DM.QuoteResponses> QuoteResponseNew(int id, int user = 0)
		{
			// TODO: Add owner filter
			if (id == 0)
			{
				log.LogWarning("Cannot process quote id = 0. Aborting");
			}
			DM.QuoteResponses loResponse = null;
			try
			{
				loResponse = await CTX.QuoteResponses
								.AsNoTracking()
								.Where(y => y.QuoteId == id)
								.Include(y => y.Quote)
								.Include(y => y.QuotedByNavigation)
								.Include(y => y.Truck)
								.Include(y => y.QuoteResponseIncludes)
								.FirstOrDefaultAsync()
								;
				#region Generated SQL - 2019-12-26 23:18:00
				/*
				-- Microsoft.EntityFrameworkCore.Diagnostics.EventDefinition
				SELECT `t`.`RESPONSE_ID`, `t`.`ADD_BY`, `t`.`ADD_ON`, `t`.`CAPITAL_ASSIST_AMT`, `t`.`CAPITAL_ASSIST_REQD`, `t`.`CONTRACT_NUM`, `t`.`EDIT_BY`, `t`.`EDIT_ON`, `t`.`LOAD_UOM`, `t`.`NOTES_DEVIATIONS`, `t`.`QUOTE_ID`, `t`.`QUOTED_BY`, `t`.`RATE_UNIT_LOAD`, `t`.`STATUS`, `t`.`TRUCK_ID`, `t`.`QUOTE_ID0`, `t`.`ADD_BY0`, `t`.`ADD_ON0`, `t`.`AGREEMENT_ID`, `t`.`CLASS_OF_GOODS`, `t`.`CONTRACT_NUM0`, `t`.`DESTINATION`, `t`.`EDIT_BY0`, `t`.`EDIT_ON0`, `t`.`INSURANCE`, `t`.`NOTE`, `t`.`ORIGIN`, `t`.`PAYMENT_TERMS`, `t`.`QUOTE_OWNER`, `t`.`RESPOND_BY`, `t`.`SPECIAL_CONDITIONS`, `t`.`STATUS0`, `t`.`TRANSPORTER_ID`, `t`.`TRUCK_TONNAGE`, `t`.`TRUCK-TYPE`, `t`.`USER_ID`, `t`.`ADD_BY1`, `t`.`ADD_ON1`, `t`.`EDIT_BY1`, `t`.`EDIT_ON1`, `t`.`EMAIL`, `t`.`FULL_NAME`, `t`.`LAST_LOGIN`, `t`.`LOGIN`, `t`.`MOBILE`, `t`.`ROLE`, `t`.`SCREEN_NAME`, `t`.`STATUS1`, `t`.`TYPE`, `t`.`USER_PWD`, `t`.`TRUCK_ID0`, `t`.`ADD_BY2`, `t`.`ADD_ON2`, `t`.`EDIT_BY2`, `t`.`EDIT_ON2`, `t`.`IS_OWN`, `t`.`MAKE`, `t`.`MODEL`, `t`.`NOTE0`, `t`.`OPERATED_BY`, `t`.`OWNER_CONTACT`, `t`.`OWNER_CONTACT_ALT`, `t`.`QUOTE_RESPONSE_ID`, `t`.`RC_CARD_FACE_1`, `t`.`RC_CARD_FACE_2`, `t`.`REGD_OWNER`, `t`.`REGN_NUM`, `t`.`STATUS2`, `t`.`TONNAGE`, `t`.`TYPE0`, `q1`.`RESPONSE_ID`, `q1`.`INCLUSIONS`, `q1`.`ADD_BY`, `q1`.`ADD_ON`, `q1`.`EDIT_BY`, `q1`.`EDIT_ON`, `q1`.`IS_INCLUDED`, `q1`.`STATUS`
				FROM (
					SELECT `q`.`RESPONSE_ID`, `q`.`ADD_BY`, `q`.`ADD_ON`, `q`.`CAPITAL_ASSIST_AMT`, `q`.`CAPITAL_ASSIST_REQD`, `q`.`CONTRACT_NUM`, `q`.`EDIT_BY`, `q`.`EDIT_ON`, `q`.`LOAD_UOM`, `q`.`NOTES_DEVIATIONS`, `q`.`QUOTE_ID`, `q`.`QUOTED_BY`, `q`.`RATE_UNIT_LOAD`, `q`.`STATUS`, `q`.`TRUCK_ID`, `q0`.`QUOTE_ID` AS `QUOTE_ID0`, `q0`.`ADD_BY` AS `ADD_BY0`, `q0`.`ADD_ON` AS `ADD_ON0`, `q0`.`AGREEMENT_ID`, `q0`.`CLASS_OF_GOODS`, `q0`.`CONTRACT_NUM` AS `CONTRACT_NUM0`, `q0`.`DESTINATION`, `q0`.`EDIT_BY` AS `EDIT_BY0`, `q0`.`EDIT_ON` AS `EDIT_ON0`, `q0`.`INSURANCE`, `q0`.`NOTE`, `q0`.`ORIGIN`, `q0`.`PAYMENT_TERMS`, `q0`.`QUOTE_OWNER`, `q0`.`RESPOND_BY`, `q0`.`SPECIAL_CONDITIONS`, `q0`.`STATUS` AS `STATUS0`, `q0`.`TRANSPORTER_ID`, `q0`.`TRUCK_TONNAGE`, `q0`.`TRUCK-TYPE`, `u`.`USER_ID`, `u`.`ADD_BY` AS `ADD_BY1`, `u`.`ADD_ON` AS `ADD_ON1`, `u`.`EDIT_BY` AS `EDIT_BY1`, `u`.`EDIT_ON` AS `EDIT_ON1`, `u`.`EMAIL`, `u`.`FULL_NAME`, `u`.`LAST_LOGIN`, `u`.`LOGIN`, `u`.`MOBILE`, `u`.`ROLE`, `u`.`SCREEN_NAME`, `u`.`STATUS` AS `STATUS1`, `u`.`TYPE`, `u`.`USER_PWD`, `l`.`TRUCK_ID` AS `TRUCK_ID0`, `l`.`ADD_BY` AS `ADD_BY2`, `l`.`ADD_ON` AS `ADD_ON2`, `l`.`EDIT_BY` AS `EDIT_BY2`, `l`.`EDIT_ON` AS `EDIT_ON2`, `l`.`IS_OWN`, `l`.`MAKE`, `l`.`MODEL`, `l`.`NOTE` AS `NOTE0`, `l`.`OPERATED_BY`, `l`.`OWNER_CONTACT`, `l`.`OWNER_CONTACT_ALT`, `l`.`QUOTE_RESPONSE_ID`, `l`.`RC_CARD_FACE_1`, `l`.`RC_CARD_FACE_2`, `l`.`REGD_OWNER`, `l`.`REGN_NUM`, `l`.`STATUS` AS `STATUS2`, `l`.`TONNAGE`, `l`.`TYPE` AS `TYPE0`
					FROM `BCP_DB`.`QUOTE_RESPONSES` AS `q`
					INNER JOIN `BCP_DB`.`QUOTE_REQUESTS` AS `q0` ON `q`.`QUOTE_ID` = `q0`.`QUOTE_ID`
					INNER JOIN `BCP_DB`.`USERS` AS `u` ON `q`.`QUOTED_BY` = `u`.`USER_ID`
					INNER JOIN `BCP_DB`.`LINE_TRUCKS` AS `l` ON `q`.`TRUCK_ID` = `l`.`TRUCK_ID`
					WHERE `q`.`QUOTE_ID` = @__id_0
					LIMIT 1
				) AS `t`
				LEFT JOIN `BCP_DB`.`QUOTE_RESPONSE_INCLUDES` AS `q1` ON `t`.`RESPONSE_ID` = `q1`.`RESPONSE_ID`
				ORDER BY `t`.`RESPONSE_ID`, `t`.`QUOTE_ID0`, `t`.`USER_ID`, `t`.`TRUCK_ID0`, `q1`.`RESPONSE_ID`, `q1`.`INCLUSIONS`
				*/
				#endregion
				if (loResponse == null)
				{
					// get the quote
					DM.QuoteRequests loReq = await CTX.QuoteRequests
											// .AsNoTracking()
											.Where(y => y.QuoteId == id)
											.Include(y => y.QuoteOwnerNavigation)
											.FirstOrDefaultAsync()
											;
					if (loReq == null)
					{
						log.LogWarning("Quote Request with id = {0} not found.", id);
						return null;
					}
					// prefill the response
					loResponse = new DM.QuoteResponses
					{
						QuoteId = id,
						Quote = loReq,
						LoadUom = (int)CDE.WEIGHT_UNIT.TONNES,
					};
				}
				// Add inclusion list
				// loResponse.IncludeOptionss = DDHelp.GetItems(CDE.DD_CATEGORIES.QUOTE_INCLUSIONS, false);
				loResponse.IncludeOptionss = CTX.DdItems
													// .AsNoTracking()
													.Where(y => y.DdiCatgId == (int)CDE.DD_CATEGORIES.QUOTE_INCLUSIONS)
													.OrderBy(y => y.DdiDispSeq)
													.ToList()
													;
				if (loResponse.Truck == null)
				{
					loResponse.Truck = new DM.LineTrucks();
				}
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error fetching quote id = {0}", id);
			}
			return loResponse;
		}

		public async Task<DM.QuoteResponses> QuoteResponseSave(DM.QuoteResponses response)
		{
			DM.QuoteResponses loResponse = null;
			DM.QuoteRequests loReq = null;
			int liRes = 0;
			try
			{
				loReq = await CTX.QuoteRequests.Where(y => y.QuoteId == response.QuoteId).FirstOrDefaultAsync();
				if (loReq != null)
				{
					loReq.Status = (int)CDE.QUOTE_STATUS.RESPONDED;
					loReq.EditOn = DateTime.UtcNow;
					liRes = await CTX.SaveChangesAsync();
				}
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error saving quote response");
			}
			return loResponse;
		}

		public async Task<List<DM.QuoteResponses>> QuoteRespondsList()
		{
			List<DM.QuoteResponses> lstResponds = new List<DM.QuoteResponses>();
			try
			{
				lstResponds = await CTX.QuoteResponses
									.AsNoTracking()
									.Include(y => y.Quote)
									.Include(y => y.QuotedByNavigation)
									.Include(y => y.QuoteResponseIncludes)
									.ToListAsync()
									;
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Error fetching quote requests");
			}
			return lstResponds;
		}

	}
}
