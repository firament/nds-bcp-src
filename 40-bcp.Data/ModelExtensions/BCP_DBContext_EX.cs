﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Pomelo.EntityFrameworkCore;


namespace bcp.Data.dbsets
{
	public partial class BCP_DBContext : DbContext
	{

		internal string DBConnectionString { get; set; }

		/// <summary>
		/// Override to create custom connetions outside of DI.
		/// Generally for Read-Only connections for reports.
		/// </summary>
		/// <param name="ConnectionString"></param>
		public BCP_DBContext(string psConnectionString)
		{
			DBConnectionString = psConnectionString;
		}

		/// <summary>
		/// Delete to other definition in file '30-d3m57T1v3.Data/dbsets/d3m57T1v2_DBContext.cs'
		/// </summary>
		/// <param name="optionsBuilder"></param>
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				optionsBuilder.UseMySql(DBConnectionString);
				optionsBuilder.EnableSensitiveDataLogging(true);
			}

		}


	}
}
