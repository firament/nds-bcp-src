﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
	public partial class QuoteRequests
	{
		[NotMapped]
		public List<Users> Transporters { get; set; }
	}
}
