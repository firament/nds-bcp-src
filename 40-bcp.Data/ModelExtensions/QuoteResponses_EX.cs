﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DM = bcp.Data.dbsets;
using CM = bcp.Common.Models;

namespace bcp.Data.dbsets
{
	public partial class QuoteResponses
	{
		// List of all active includes in the system
		[NotMapped]
		public virtual ICollection<DM.DdItems> IncludeOptionss { get; set; }

		// // For recieving selected options back
		// [NotMapped]
		// public List<int> IncludeOpts = new List<int>();
	}
}
