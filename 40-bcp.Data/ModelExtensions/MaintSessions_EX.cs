﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CC = bcp.Common;

namespace bcp.Data.dbsets
{
	public partial class MaintSessions
	{
		public MaintSessions()
		{
			DeviceSig = "NOT YET IMPLEMENTED";
		}
		public MaintSessions(CC.UserPayload upl)
		{
			Token = upl.Token;
			DeviceSig = "NOT YET IMPLEMENTED";
			Userid = upl.Userid;
			UserLogin = upl.Login;
			UserName = upl.DisplayName;
			UserType = (int)upl.Type;
			UserRole = (int)upl.Role;
			IssueTime = upl.IssueTime;
			LastUsed = upl.LastUsed;
			Status = (int)upl.Status;
			User_Payload = upl;
		}
		public void FromUPL(CC.UserPayload upl)
		{
			DeviceSig = upl.DeviceSig;
			Userid = upl.Userid;
			UserLogin = upl.Login;
			UserName = upl.DisplayName;
			UserType = (int)upl.Type;
			UserRole = (int)upl.Role;
			IssueTime = upl.IssueTime;
			LastUsed = upl.LastUsed;
			AuthTime = upl.AuthTime ?? DateTimeOffset.MinValue;
			Status = (int)upl.Status;
			User_Payload = upl;
		}
		// public string Payload { get; set; }

		[NotMapped]
		private CC.UserPayload _upl = null;
		[NotMapped]
		public CC.UserPayload User_Payload
		{
			get
			{
				if (string.IsNullOrWhiteSpace(this.PayloadData))
				{
					return null;
				}
				if (_upl == null)
				{
					_upl = CC.UserPayload.FromJSON(this.PayloadData);
				}
				return this._upl;
			}
			set
			{
				_upl = value;
				this.PayloadData = _upl.ToJson(false);
			}
		}
	}
}
