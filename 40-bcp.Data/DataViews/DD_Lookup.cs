#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x
#endregion // Using Declarations
using Microsoft.EntityFrameworkCore;

namespace bcp.Data.Views
{
	public class DD_Lookup
	{
		internal readonly ILogger<DD_Lookup> log;
		internal readonly CC.IAppsettings APPCFG;
		private bool IsInitialized { get; set; } = false;
		private Dictionary<int, List<DM.DdItems>> LookupItems;
		private int STATUS_ACTIVE = -1;

		public DD_Lookup(ILogger<DD_Lookup> _logger, CC.IAppsettings _app_cfg)
		{
			log = _logger;
			APPCFG = _app_cfg;
		}

		public List<CM.DD_Item> GetItems(CDE.DD_CATEGORIES Catg, bool ActiveOnly = true)
		{
			if (!IsInitialized) { Initialize(); }
			if (!IsInitialized)
			{
				log.LogError("No data to process.");
				return null;
			}

			List<DM.DdItems> lstItems = null;
			lstItems = ActiveOnly
					? LookupItems[(int)Catg].Where(a => a.Status == (int)CDE.RECORD_STATUS.ACTIVE).ToList()
					: LookupItems[(int)Catg]
					;

			List<CM.DD_Item> lstOptions = new List<CM.DD_Item>();
			foreach (DM.DdItems vItem in lstItems.OrderBy(ds => ds.DdiDispSeq))
			{
				lstOptions.Add(new CM.DD_Item()
				{
					CatgID = vItem.DdiCatgId,
					Value = vItem.DdiCode,
					DisplaySeq = vItem.DdiDispSeq,
					Text = vItem.DdiDispText,
					Label = vItem.DdiDesc,
					Active = vItem.Status == (int)CDE.RECORD_STATUS.ACTIVE
				}
				);
			}
			return lstOptions;
		}

		// TODO: wrap inactive items with '[]'
		public string DisplayText(int CatgID, int ItemID, bool IncludeInactive = false)
		{
			if (CatgID == 7)
			{
				log.LogDebug("Watch");
			}
			if (!IsInitialized) { Initialize(); }
			if (!IsInitialized)
			{
				log.LogError("No data to process.");
				return string.Empty;
			}

			string lsDisplayText = string.Empty;
			lsDisplayText = IncludeInactive
				? LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID)?.DdiDispText
				: LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID && ic.Status == STATUS_ACTIVE)?.DdiDispText
				;
			if (string.IsNullOrWhiteSpace(lsDisplayText))
			{
				log.LogError("No entries defined for Item ID {0} with Category ID {1} [IncludeInactive = {2}]", ItemID, CatgID, IncludeInactive);
			}
			return lsDisplayText;
		}


		private void Initialize()
		{
			log.LogDebug("==> Initialize");
			/*
			Get all lookups
			 */
			if (IsInitialized)
			{
				return;
			}

			STATUS_ACTIVE = (int)CDE.RECORD_STATUS.ACTIVE;
			List<DM.DdCategories> lstCatgs = null;
			LookupItems = new Dictionary<int, List<DM.DdItems>>();

			try
			{
				// Get all categories, along with items
				using (CTX loCTX = new CTX(APPCFG.DynamicData.DBConnRO))
				{
					lstCatgs = loCTX.DdCategories.AsNoTracking().Include(y => y.DdItems).OrderBy(y => y.DdiCatgId).ToList();
				}
			}
			catch (System.Exception se)
			{
				log.LogError(se, "Failed to get lookup data from database.");
			}

			if (lstCatgs == null || lstCatgs.Count == 0)
			{
				log.LogError("Lookup data not availaible. Unrecoverable error. Aborting further processing.");
				return;
			}

			int liCatgCode = 0;
			List<DM.DdItems> vCatgItems;
			foreach (DM.DdCategories vCat in lstCatgs)
			{
				liCatgCode = vCat.DdiCatgId;
				vCatgItems = new List<DM.DdItems>();
				foreach (DM.DdItems vItm in vCat.DdItems.OrderBy(y => y.DdiDispSeq))
				{
					vCatgItems.Add(new DM.DdItems
					{
						DdiCatgId = liCatgCode,
						DdiCode = vItm.DdiCode,
						DdiDispSeq = vItm.DdiDispSeq,
						DdiDispText = vItm.DdiDispText,
						DdiDesc = vItm.DdiDesc,
						Status = vItm.Status,
					});
				}
				LookupItems.Add(liCatgCode, vCatgItems);
			}
			IsInitialized = true;
		}

	}
}
