﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("TRUCKS", Schema = "BCP_DB")]
    public partial class Trucks
    {
        [Key]
        [Column("TRUCK_ID", TypeName = "int(11)")]
        public int TruckId { get; set; }
        [Column("OPERATED_BY", TypeName = "int(11)")]
        public int OperatedBy { get; set; }
        [Required]
        [Column("REGN_NUM")]
        [StringLength(20)]
        public string RegnNum { get; set; }
        [Column("MAKE_MODEL")]
        [StringLength(60)]
        public string MakeModel { get; set; }
        [Column("MODEL", TypeName = "int(11)")]
        public int Model { get; set; }
        [Column("TONNAGE", TypeName = "int(11)")]
        public int Tonnage { get; set; }
        [Column("TYPE", TypeName = "int(11)")]
        public int Type { get; set; }
        [Column("IS_OWN", TypeName = "int(11)")]
        public int IsOwn { get; set; }
        [Required]
        [Column("REGD_OWNER")]
        [StringLength(60)]
        public string RegdOwner { get; set; }
        [Column("OWNER_CONTACT")]
        [StringLength(16)]
        public string OwnerContact { get; set; }
        [Column("OWNER_CONTACT_ALT")]
        [StringLength(16)]
        public string OwnerContactAlt { get; set; }
        [Required]
        [Column("RC_CARD_FACE_1")]
        [StringLength(1020)]
        public string RcCardFace1 { get; set; }
        [Required]
        [Column("RC_CARD_FACE_2")]
        [StringLength(1020)]
        public string RcCardFace2 { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("OperatedBy")]
        [InverseProperty("Trucks")]
        public virtual Users OperatedByNavigation { get; set; }
    }
}
