﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("QUOTE_RESPONSES", Schema = "BCP_DB")]
    public partial class QuoteResponses
    {
        public QuoteResponses()
        {
            LineTrucks = new HashSet<LineTrucks>();
            QuoteResponseIncludes = new HashSet<QuoteResponseIncludes>();
        }

        [Key]
        [Column("RESPONSE_ID", TypeName = "int(11)")]
        public int ResponseId { get; set; }
        [Column("QUOTE_ID", TypeName = "int(11)")]
        public int QuoteId { get; set; }
        [Column("QUOTED_BY", TypeName = "int(11)")]
        public int QuotedBy { get; set; }
        [Column("TRUCK_ID", TypeName = "int(11)")]
        public int TruckId { get; set; }
        [Required]
        [Column("CONTRACT_NUM")]
        [StringLength(255)]
        public string ContractNum { get; set; }
        [Column("LOAD_UOM", TypeName = "int(11)")]
        public int LoadUom { get; set; }
        [Column("RATE_UNIT_LOAD", TypeName = "decimal(7,2)")]
        public decimal RateUnitLoad { get; set; }
        [Column("NOTES_DEVIATIONS")]
        [StringLength(1020)]
        public string NotesDeviations { get; set; }
        [Column("CAPITAL_ASSIST_REQD", TypeName = "int(11)")]
        public int CapitalAssistReqd { get; set; }
        [Column("CAPITAL_ASSIST_AMT", TypeName = "decimal(7,2)")]
        public decimal CapitalAssistAmt { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        public virtual QuoteRequests ContractNumNavigation { get; set; }
        [ForeignKey("QuoteId")]
        [InverseProperty("QuoteResponsesQuote")]
        public virtual QuoteRequests Quote { get; set; }
        [ForeignKey("QuotedBy")]
        [InverseProperty("QuoteResponses")]
        public virtual Users QuotedByNavigation { get; set; }
        [ForeignKey("TruckId")]
        [InverseProperty("QuoteResponses")]
        public virtual LineTrucks Truck { get; set; }
        [InverseProperty("QuoteResponse")]
        public virtual ICollection<LineTrucks> LineTrucks { get; set; }
        [InverseProperty("Response")]
        public virtual ICollection<QuoteResponseIncludes> QuoteResponseIncludes { get; set; }
    }
}
