﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("MAINT_USER_VERIFY", Schema = "BCP_DB")]
    public partial class MaintUserVerify
    {
        [Key]
        [Column("MUV_ID", TypeName = "int(11)")]
        public int MuvId { get; set; }
        [Column("MOBILE", TypeName = "int(11)")]
        public int Mobile { get; set; }
        [Required]
        [Column("VERIFY_PIN", TypeName = "char(6)")]
        public string VerifyPin { get; set; }
        [Required]
        [Column("PAYLOAD")]
        public string Payload { get; set; }
        [Column("NOTE")]
        public string Note { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
    }
}
