﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("LEGAL_DOCUMENTS", Schema = "BCP_DB")]
    public partial class LegalDocuments
    {
        public LegalDocuments()
        {
            UserDocumentMap = new HashSet<UserDocumentMap>();
        }

        [Key]
        [Column("DOCUMENT_ID", TypeName = "int(11)")]
        public int DocumentId { get; set; }
        [Column("DOC_NUMBER")]
        [StringLength(60)]
        public string DocNumber { get; set; }
        [Required]
        [Column("FILE_NAME_SYS")]
        [StringLength(60)]
        public string FileNameSys { get; set; }
        [Required]
        [Column("FILE_NAME_USER")]
        [StringLength(60)]
        public string FileNameUser { get; set; }
        [Required]
        [Column("VERSION")]
        [StringLength(8)]
        public string Version { get; set; }
        [Column("TYPE", TypeName = "int(11)")]
        public int Type { get; set; }
        [Column("MIME_TYPE", TypeName = "int(11)")]
        public int MimeType { get; set; }
        [Column("DOC_HASH")]
        public string DocHash { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [InverseProperty("Document")]
        public virtual ICollection<UserDocumentMap> UserDocumentMap { get; set; }
    }
}
