﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("GEN_APP_SETTINGS", Schema = "BCP_DB")]
    public partial class GenAppSettings
    {
        [Column("ID", TypeName = "int(11)")]
        public int Id { get; set; }
        [Required]
        [Column("APP_CODE")]
        [StringLength(8)]
        public string AppCode { get; set; }
        [Column("APP_CONFIG_VER", TypeName = "int(2)")]
        public int AppConfigVer { get; set; }
        [Required]
        [Column("APP_CONFIG")]
        public string AppConfig { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }
    }
}
