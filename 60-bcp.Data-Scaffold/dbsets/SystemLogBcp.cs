﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("SYSTEM_LOG_BCP", Schema = "BCP_DB")]
    public partial class SystemLogBcp
    {
        [Column("ID", TypeName = "bigint(20) unsigned")]
        public long Id { get; set; }
        [Column("LOG_TIME")]
        public DateTimeOffset LogTime { get; set; }
        [Required]
        [Column("APP_CODE")]
        [StringLength(12)]
        public string AppCode { get; set; }
        [Required]
        [Column("MACHINE")]
        [StringLength(60)]
        public string Machine { get; set; }
        [Column("ACTIVITY_ID")]
        [StringLength(40)]
        public string ActivityId { get; set; }
        [Required]
        [Column("LOG_LEVEL")]
        [StringLength(8)]
        public string LogLevel { get; set; }
        [Column("LOGGER")]
        [StringLength(250)]
        public string Logger { get; set; }
        [Required]
        [Column("MESSAGE")]
        public string Message { get; set; }
        [Required]
        [Column("CALL_SITE")]
        [StringLength(1020)]
        public string CallSite { get; set; }
        [Column("EXCEPTION")]
        public string Exception { get; set; }
        [Column("STACKTRACE")]
        public string Stacktrace { get; set; }
        [Column("ALL_EVENT_PROPS")]
        public string AllEventProps { get; set; }
    }
}
