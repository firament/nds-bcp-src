﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("QUOTE_REQUESTS", Schema = "BCP_DB")]
    public partial class QuoteRequests
    {
        public QuoteRequests()
        {
            QuoteResponsesContractNumNavigation = new HashSet<QuoteResponses>();
            QuoteResponsesQuote = new HashSet<QuoteResponses>();
        }

        [Key]
        [Column("QUOTE_ID", TypeName = "int(11)")]
        public int QuoteId { get; set; }
        [Column("QUOTE_OWNER", TypeName = "int(11)")]
        public int QuoteOwner { get; set; }
        [Required]
        [Column("CONTRACT_NUM")]
        [StringLength(255)]
        public string ContractNum { get; set; }
        [Column("TRANSPORTER_ID", TypeName = "int(11)")]
        public int TransporterId { get; set; }
        [Required]
        [Column("ORIGIN")]
        [StringLength(60)]
        public string Origin { get; set; }
        [Required]
        [Column("DESTINATION")]
        [StringLength(60)]
        public string Destination { get; set; }
        [Column("TRUCK-TYPE", TypeName = "int(11)")]
        public int TruckType { get; set; }
        [Column("TRUCK_TONNAGE", TypeName = "int(11)")]
        public int TruckTonnage { get; set; }
        [Column("CLASS_OF_GOODS", TypeName = "int(11)")]
        public int ClassOfGoods { get; set; }
        [Required]
        [Column("SPECIAL_CONDITIONS")]
        [StringLength(1020)]
        public string SpecialConditions { get; set; }
        [Required]
        [Column("PAYMENT_TERMS")]
        [StringLength(1020)]
        public string PaymentTerms { get; set; }
        [Required]
        [Column("INSURANCE")]
        [StringLength(1020)]
        public string Insurance { get; set; }
        [Required]
        [Column("RESPOND_BY")]
        [StringLength(24)]
        public string RespondBy { get; set; }
        [Column("AGREEMENT_ID")]
        [StringLength(60)]
        public string AgreementId { get; set; }
        [Column("NOTE")]
        [StringLength(1020)]
        public string Note { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("QuoteOwner")]
        [InverseProperty("QuoteRequestsQuoteOwnerNavigation")]
        public virtual Users QuoteOwnerNavigation { get; set; }
        [ForeignKey("TransporterId")]
        [InverseProperty("QuoteRequestsTransporter")]
        public virtual Users Transporter { get; set; }
        public virtual ICollection<QuoteResponses> QuoteResponsesContractNumNavigation { get; set; }
        [InverseProperty("Quote")]
        public virtual ICollection<QuoteResponses> QuoteResponsesQuote { get; set; }
    }
}
