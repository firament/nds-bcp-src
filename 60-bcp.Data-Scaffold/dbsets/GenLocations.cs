﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("GEN_LOCATIONS", Schema = "BCP_DB")]
    public partial class GenLocations
    {
        [Key]
        [Column("LOCATION_ID", TypeName = "int(11)")]
        public int LocationId { get; set; }
        [Column("IS_CUSTOM", TypeName = "int(11)")]
        public int IsCustom { get; set; }
        [Required]
        [Column("LOC_FULL")]
        [StringLength(512)]
        public string LocFull { get; set; }
        [Required]
        [Column("TOWN")]
        [StringLength(60)]
        public string Town { get; set; }
        [Required]
        [Column("CITY")]
        [StringLength(60)]
        public string City { get; set; }
        [Required]
        [Column("STATE")]
        [StringLength(60)]
        public string State { get; set; }
        [Required]
        [Column("COUNTRY")]
        [StringLength(60)]
        public string Country { get; set; }
        [Required]
        [Column("STATE_CODE")]
        [StringLength(8)]
        public string StateCode { get; set; }
        [Required]
        [Column("COUNTRY_CODE")]
        [StringLength(8)]
        public string CountryCode { get; set; }
        [Column("IS_CITY", TypeName = "int(11)")]
        public int IsCity { get; set; }
        [Column("PIN_CODES")]
        public string PinCodes { get; set; }
        [Column("LOC_LAT", TypeName = "decimal(8,8)")]
        public decimal? LocLat { get; set; }
        [Column("LOC_LON", TypeName = "decimal(8,8)")]
        public decimal? LocLon { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }
    }
}
