﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bcp.Data.dbsets
{
    [Table("USERS", Schema = "BCP_DB")]
    public partial class Users
    {
        public Users()
        {
            LineTrucks = new HashSet<LineTrucks>();
            QuoteRequestsQuoteOwnerNavigation = new HashSet<QuoteRequests>();
            QuoteRequestsTransporter = new HashSet<QuoteRequests>();
            QuoteResponses = new HashSet<QuoteResponses>();
            Trucks = new HashSet<Trucks>();
            UserAddresses = new HashSet<UserAddresses>();
            UserDocumentMap = new HashSet<UserDocumentMap>();
        }

        [Key]
        [Column("USER_ID", TypeName = "int(11)")]
        public int UserId { get; set; }
        [Required]
        [Column("FULL_NAME")]
        [StringLength(60)]
        public string FullName { get; set; }
        [Column("SCREEN_NAME")]
        [StringLength(24)]
        public string ScreenName { get; set; }
        [Required]
        [Column("LOGIN")]
        [StringLength(40)]
        public string Login { get; set; }
        [Required]
        [Column("USER_PWD")]
        public string UserPwd { get; set; }
        [Column("TYPE", TypeName = "int(11)")]
        public int Type { get; set; }
        [Column("ROLE", TypeName = "int(11)")]
        public int Role { get; set; }
        [Required]
        [Column("MOBILE")]
        [StringLength(16)]
        public string Mobile { get; set; }
        [Column("EMAIL")]
        [StringLength(40)]
        public string Email { get; set; }
        [Column("LAST_LOGIN")]
        public DateTimeOffset LastLogin { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(11)")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(11)")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [InverseProperty("User")]
        public virtual UserProfiles UserProfiles { get; set; }
        [InverseProperty("OperatedByNavigation")]
        public virtual ICollection<LineTrucks> LineTrucks { get; set; }
        [InverseProperty("QuoteOwnerNavigation")]
        public virtual ICollection<QuoteRequests> QuoteRequestsQuoteOwnerNavigation { get; set; }
        [InverseProperty("Transporter")]
        public virtual ICollection<QuoteRequests> QuoteRequestsTransporter { get; set; }
        [InverseProperty("QuotedByNavigation")]
        public virtual ICollection<QuoteResponses> QuoteResponses { get; set; }
        [InverseProperty("OperatedByNavigation")]
        public virtual ICollection<Trucks> Trucks { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<UserAddresses> UserAddresses { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<UserDocumentMap> UserDocumentMap { get; set; }
    }
}
