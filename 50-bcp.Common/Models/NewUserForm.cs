using System;
using System.ComponentModel.DataAnnotations;

namespace bcp.Common.Models
{
	public class NewUserForm
	{
		[Required]
		[MinLength(10)]
		[MaxLength(10)]
		public string Login { get; set; }
		[Required]
		[MinLength(6)]
		[MaxLength(24)]
		public string Password { get; set; }
		public string ConfirmOTP { get; set; }
		[Required]
		public string Token { get; set; }
		public DateTime Posted { get; set; }
		public DateTime Confirmed { get; set; }
		public int Status { get; set; }
	}
}
