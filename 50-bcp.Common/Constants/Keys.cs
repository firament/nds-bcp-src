using System;
namespace bcp.Common.Constants
{
	public static class TAGS
	{
		public const string OPTION_DISABLED = "cstm-option-disabled";
	}

	public static class KEYS
	{
		public const int ANON_USER_ID = 1;
		public const string ANON_USER_NAME = "ANONYMOUS";
		public const int SYSTEM_USER_ID_VAL = 2;
		public const string TOKEN_KEY = "bcp-api-key";
	}

}

