namespace bcp.Common.Constants
{
	public class ErrorCodeList
	{
		public const int ALL_OK = 11;

		public const int FORM_EMPTY = 1001;
		public const int FORM_INCOMPLETE = 1010;
		public const int FEILD_MISSING_EMPTY_BAD = 1012;
		public const int USER_PROFILE_PARTIAL = 1020;

		public const int SESSIONLOG_MISSING = 1020;
		public const int SESSIONLOG_BROKEN = 1030;

		public const int DB_NO_RESULT = 5000;
		public const int DB_INSERT_ERROR = 5010;
		public const int DB_FETCH_ERROR = 5010;
		public const int DB_UPDATE_ERROR = 5020;

	}
}
