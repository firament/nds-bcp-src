using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NLog;
using J = Newtonsoft.Json;

using System.IO;

namespace bcp.Common
{
	public static class Utils
	{
		static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		/// <summary>
		/// 
		/// </summary>
		public static Newtonsoft.Json.JsonSerializerSettings JsonSettingsDefault
		{
			get
			{
				return new Newtonsoft.Json.JsonSerializerSettings()
				{
					Formatting = Newtonsoft.Json.Formatting.None,
					NullValueHandling = Newtonsoft.Json.NullValueHandling.Include,
					MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore,
					StringEscapeHandling = Newtonsoft.Json.StringEscapeHandling.EscapeNonAscii,
					//ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
				};
			}
		}

		/// <summary>
		/// Removes malicious characters from input string. User should compare length of input and output to flag malicious content
		/// </summary>
		/// <param name="psInput">String data to scrub</param>
		/// <param name="pbIsNullable">Flag to return null if entity feild is nullable</param>
		/// <returns>input string with all black listed characters removed.</returns>
		/// <remarks>from CAWEB.Core</remarks>
		public static string SQL_Input_Scrub(string psInput, bool pbIsNullable = false)
		{
			if (string.IsNullOrWhiteSpace(psInput))
			{
				if (pbIsNullable) return null;
				else return string.Empty;
			}
			return psInput
				.Trim()
				.Replace("--", "")
				.Replace("*", "")
				.Replace(";", "")
				.Replace("%", "")
				.Replace("<", "")
				.Replace(">", "")
				.Replace("=", "")
				;
			// .Replace("\n", "<br>") do an htmlencode
		}


		public static string ToJSON(Object obj, bool pPrettyPrint = true)
		{
			if (obj == null)
			{
				log.Warn("Cannot serialize a Null object");
				return string.Empty;
			}
			string lsJSON = string.Empty;
			Newtonsoft.Json.JsonSerializerSettings loSettings = JsonSettingsDefault;
			loSettings.Formatting =
						pPrettyPrint
						? Newtonsoft.Json.Formatting.Indented
						: Newtonsoft.Json.Formatting.None
						;
			try
			{
				lsJSON = J.JsonConvert.SerializeObject(obj, loSettings);
			}
			catch (Newtonsoft.Json.JsonSerializationException ExSEX)
			{
				log.Warn(ExSEX, "Parsing Error, Trying with additional settings.");
				loSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
				lsJSON = ToJSON(obj, loSettings);
			}
			catch (System.Exception ExSE)
			{
				log.Error(ExSE, "Unexpected error while parsing JSON");
			}
			return lsJSON;
		}

		public static string ToJSON(Object obj, Newtonsoft.Json.JsonSerializerSettings pSettings)
		{
			if (obj == null)
			{
				log.Warn("Cannot serialize a Null object");
				return string.Empty;
			}
			string lsJSON = string.Empty;
			Newtonsoft.Json.JsonSerializerSettings loSettings = pSettings ?? new J.JsonSerializerSettings();
			try
			{
				lsJSON = J.JsonConvert.SerializeObject(obj, loSettings);
			}
			catch (System.Exception ExSE)
			{
				log.Error(ExSE, "Unexpected error while parsing JSON");
			}
			return lsJSON;
		}


		[Obsolete("Avoid requiring consumeer needing reference to Newtonsoft.Json", true)]
		public static string ToJSON(Object obj, J.Formatting pFormat = J.Formatting.None)
		{
			if (obj == null)
			{
				log.Warn("Cannot serialize a Null object");
				return string.Empty;
			}
			string lsJSON = string.Empty;
			try
			{
				lsJSON = J.JsonConvert.SerializeObject(obj, pFormat);
			}
			catch (System.Exception ExSE)
			{
				log.Error(ExSE, "Unexpected error while parsing JSON");
			}
			return lsJSON;
		}

		public static object FromJSON(string psJsonText, Type pType)
		{
			object dsObj = null;
			if (string.IsNullOrWhiteSpace(psJsonText))
			{
				log.Error("Input string is not proper. {0}", psJsonText);
				return dsObj;
			}
			if (pType == null)
			{
				log.Error("Cannot convert without type information");
			}

			// log.Debug("Converting to object Type {0}", pType.FullName);
			try
			{
				dsObj = J.JsonConvert.DeserializeObject(psJsonText, pType);
				log.Debug("Conversion is Successful, to object Type {0}", dsObj.GetType().FullName);
			}
			catch (Exception eX)
			{
				log.Fatal(eX, "Unexpected error on deserializing Json string to Object");
			}
			return dsObj;
		}


		public static string GetCodeShort()
		{
			string lsCode = Guid.NewGuid().ToString();
			// TODO: generate a short code
			/*
			| Specifier | Format of return value                                 | Sample                                 |
			|-----------|--------------------------------------------------------|----------------------------------------|
			| N         | 32 digits                                              | 00000000000000000000000000000000       |
			| D         | 32 digitsseparated by hyphens                          | 00000000-0000-0000-0000-000000000000   |
			| B         | 32 digitsseparated by hyphens, enclosed in braces      | {00000000-0000-0000-0000-000000000000} |
			| P         | 32 digitsseparated by hyphens, enclosed in parentheses | (00000000-0000-0000-0000-000000000000) |
			 */
			return lsCode;
		}


		public static string GetHashedPWD(string psPWD_Text)
		{
			if (string.IsNullOrWhiteSpace(psPWD_Text))
				return psPWD_Text;

			string lsPwdTxt = psPWD_Text;
			System.Security.Cryptography.SHA256 sha256 = new System.Security.Cryptography.SHA256Managed();
			byte[] sha256Bytes = System.Text.Encoding.Default.GetBytes(lsPwdTxt);
			byte[] cryString = sha256.ComputeHash(sha256Bytes);
			string sha256Str = string.Empty;
			for (int i = 0; i < cryString.Length; i++)
			{
				sha256Str += cryString[i].ToString("X"); // CA1305 - NOT APPLICABLE FOR SECURITY SCAN
														 // use string builder,
														 // also check base64 functions to convert entire array to single string
														 //	may need to remove '=' chars
			}
			return sha256Str;
		}

		public static TimeSpan TimeSpanFromMinutes(int minutes)
		{
			// keep dividing minutes by 60 to get hours
			int liHours = minutes / 60;
			int liMins = minutes % 60;
			log.Debug("Converted {0} Minutes to HH:MM {1}:{2}", minutes, liHours, liMins);
			return new TimeSpan(liHours, liMins, 0);
		}

	}   // public static class Utils
}   // namespace bcp.common
