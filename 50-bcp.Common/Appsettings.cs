using System;
using NJ = Newtonsoft.Json;

namespace bcp.Common
{
	public class Appsettings : IAppsettings
	{
		public Appsettings()
		{
			DynamicData = new DynamicSettings();
			SessionOpts = new SessionSettings();
		}
		[NJ.JsonIgnore]
		public DateTime LastLoaded { get; set; }
		public string AppCode { get; set; }
		public int AppConfigVer { get; set; }
		public string UploadLocation { get; set; }
		public IDisplayOptions DisplayOpts { get; set; }
		public ISessionSettings SessionOpts { get; set; }
		[NJ.JsonIgnore]
		public IDynamicSettings DynamicData { get; set; }
	}
	public class DisplayOptions : IDisplayOptions
	{
		public string DateTimeFixed { get; set; }
		public string LatLon { get; set; }
	}
	public class DynamicSettings : IDynamicSettings
	{
		public string DBConnRW { get; set; }
		public string DBConnRO { get; set; }
	}
	public class SessionSettings : ISessionSettings
	{
		public string AuthSchemeName { get; set; } = "BCPIdentityManager";
		public string AuthCookieName { get; set; } = "bcpauth";
		public string AuthCookieDomain { get; set; } = "localhost:5031";
		public string AccessDenyPath { get; set; } = "/Session/Login/";
		public string SignOnPath { get; set; } = "/Session/Login/";
		public string SignOutPath { get; set; } = "/Session/Logout/";
		public string ReturnURL_TagName { get; set; } = "bcpAuthFailURL";
		public int SessionTimeoutMinutes { get; set; } = 30;
		public int ANON_USER_ID { get; set; } = 1;
		public string ANON_USER_NAME { get; set; } = "ANONYMOUS";
		public string ANON_LOGIN { get; set; } = "1_1_1_1_1_1_1_1_1_1_";
	}

	public interface IAppsettings
	{
		[NJ.JsonIgnore]
		DateTime LastLoaded { get; set; }
		string AppCode { get; set; }
		int AppConfigVer { get; set; }
		string UploadLocation { get; set; }
		IDisplayOptions DisplayOpts { get; set; }
		IDynamicSettings DynamicData { get; set; }
		ISessionSettings SessionOpts { get; set; }
	}
	public interface IDisplayOptions
	{
		string DateTimeFixed { get; set; }
		string LatLon { get; set; }
	}
	public interface IDynamicSettings
	{
		string DBConnRW { get; set; }
		string DBConnRO { get; set; }
	}

	public interface ISessionSettings
	{
		string AuthSchemeName { get; set; }
		string AuthCookieName { get; set; }
		string AuthCookieDomain { get; set; }
		string AccessDenyPath { get; set; }
		string SignOnPath { get; set; }
		string SignOutPath { get; set; }
		string ReturnURL_TagName { get; set; }
		int SessionTimeoutMinutes { get; set; }
		int ANON_USER_ID { get; set; }
		string ANON_USER_NAME { get; set; }
		string ANON_LOGIN { get; set; }
	}

}
