using System;
using System.Runtime.Serialization;
using CDE = bcp.Common.DDEnums;

namespace bcp.Common
{
	// Clone of gpm.Efc.dbsets.MaintSessions
	public class UserPayload
	{
		public long Msid { get; set; }
		public string Token { get; set; }
		public string DeviceSig { get; set; }
		public int Userid { get; set; }
		public string Login { get; set; }
		public string DisplayName { get; set; }
		public CDE.USER_ROLE Role { get; set; }
		public CDE.USER_TYPE Type { get; set; }
		public DateTimeOffset IssueTime { get; set; }
		public DateTimeOffset? AuthTime { get; set; }
		public DateTimeOffset LastUsed { get; set; }
		public bcp.Common.DDEnums.SESSION_STATUS Status { get; set; }


		/* Constructors */
		public UserPayload() { }

		public string ToJson(bool Formatted = true)
		{
			string lsJSON = Utils.ToJSON(this, Formatted);
			return lsJSON;
		}
		public static UserPayload FromJSON(string JSONString)
		{
			UserPayload loUPL;
			loUPL = Utils.FromJSON(JSONString, typeof(UserPayload)) as UserPayload;
			return loUPL;
		}
	}
}
