using System;
using System.Collections.Generic;
using NJ = Newtonsoft.Json;

/*
Error Code ranges
1000	1999	General
2000	2999	admin
3000	3999	Anchor_Client
4000	4999	Transporter
5000	5999	Booking_Agent
6000	6999	Lender
7000	7999	Driver
8000	8999	Service_Provider
*/


namespace bcp.Common.ErrorModels
{

	public partial class ErrorCodeFactory : IErrorCodeFactory
	{

		/// <summary>
		/// 
		/// </summary>
		internal Dictionary<int, IErrorCode> mErrorCodes = null;

		public ErrorCodeFactory()
		{
			mErrorCodes = new Dictionary<int, IErrorCode>();
			// Initialize all error codes
			GeneralErrorList().ForEach(vEC => mErrorCodes.Add(vEC.Code, vEC));
			AdminErrorList_20().ForEach(vEC => mErrorCodes.Add(vEC.Code, vEC));

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="statuscode"></param>
		/// <param name="CustomMessage"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public IErrorCode getErrorCode(int statuscode, string CustomMessage = null, params string[] args)
		{
			IErrorCode loErr = null;
			if (!mErrorCodes.ContainsKey(statuscode))
			{
				return new ErrorCode
				{
					Code = 0,
					ErrorMesg = "Unspecified error. Args are: " + string.Join(", ", args)
				};
			}

			loErr = mErrorCodes[statuscode];

			// TODO: Use try-catch to avoid formatting errors.
			if (args.Length > 0 && !string.IsNullOrWhiteSpace(loErr.MesgTemplate))
			{
				if (!string.IsNullOrWhiteSpace(CustomMessage))
				{
					loErr.ErrorMesg = string.Format(CustomMessage, args);
				}
				else
				{
					loErr.ErrorMesg = string.Format(loErr.MesgTemplate, args);
				}
				return loErr;
			}

			if (!string.IsNullOrWhiteSpace(CustomMessage))
			{
				loErr.ErrorMesg = CustomMessage;
			}

			return loErr;
		}

		// Dump list of all defined errors in the system
		public string ToJSON()
		{
			string lsJsonString = null;
			lsJsonString = Utils.ToJSON(this, false);
			return lsJsonString;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public interface IErrorCodeFactory
	{
		// IErrorCode getErrorCode(int statuscode, params string[] args);
		IErrorCode getErrorCode(int statuscode, string CustomMessage = null, params string[] args);
		string ToJSON();
	}

	/// <summary>
	/// 
	/// </summary>
	public interface IErrorCode
	{
		int Code { get; set; }
		string ErrorTrackID { get; set; }
		string ErrorMesg { get; set; }
		[NJ.JsonIgnore]
		string MesgTemplate { get; set; }
		string ToJSON();
	}

	/// <summary>
	/// 
	/// </summary>
	public class ErrorCode : IErrorCode
	{
		public int Code { get; set; }
		public string ErrorTrackID { get; set; } = Guid.NewGuid().ToString();
		public string ErrorMesg { get; set; }
		[NJ.JsonIgnore]
		public string MesgTemplate { get; set; }
		public string ToJSON()
		{
			string lsJsonString = null;
			lsJsonString = Utils.ToJSON(this, false);
			return lsJsonString;
		}
	}




}
