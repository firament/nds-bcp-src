using System.Collections.Generic;
using CAEL = bcp.Common.Constants.ErrorCodeList;
/*
Error Code ranges

1000	1999	General
1000	1050	Startup
1100	1199	Session

*/

namespace bcp.Common.ErrorModels
{

	public partial class ErrorCodeFactory : IErrorCodeFactory
	{
		internal List<IErrorCode> TestErrors() => new List<IErrorCode>
		{
			new ErrorCode
			{
				Code = 10,
				ErrorMesg = "10 - Unexpected and unspecified Error",
				MesgTemplate = string.Empty,
			},
			new ErrorCode
			{
				Code = 20,
				ErrorMesg = "20 - Unexpected and unspecified Error",
				MesgTemplate = string.Empty,
			},
			new ErrorCode
			{
				Code = 30,
				ErrorMesg = "30 - Unexpected and unspecified Error",
				MesgTemplate = "{0} - Unexpected and unspecified Error occurred non date {1}",
			},
		};

		internal List<IErrorCode> GeneralErrorList() => new List<IErrorCode>
		{
			new ErrorCode
			{
				Code = CAEL.FORM_EMPTY,
				ErrorMesg = "Empty Form submitted",
				MesgTemplate = string.Empty,
			},
			new ErrorCode
			{
				Code = CAEL.FORM_INCOMPLETE,
				ErrorMesg = "Form submitted with missing data. Unprocessable.",
				MesgTemplate = string.Empty,
			},
			new ErrorCode
			{
				Code = CAEL.FEILD_MISSING_EMPTY_BAD,
				ErrorMesg = "A required feild is missing, or is empty or had unexpected value. Unprocessable.",
				MesgTemplate = string.Empty,
			},
			new ErrorCode
			{
				Code = CAEL.SESSIONLOG_MISSING,
				ErrorMesg = "Session log entry missing. Unprocessable.",
				MesgTemplate = string.Empty,
			},
			new ErrorCode
			{
				Code = CAEL.SESSIONLOG_BROKEN,
				ErrorMesg = "Session log entry integerity is compromised. Unprocessable.",
				MesgTemplate = string.Empty,
			},
		};

	}





}
