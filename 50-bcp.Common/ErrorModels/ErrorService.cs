using System.Collections.Generic;

namespace bcp.Common.ErrorModels
{
	public interface IErrorService
	{
		IErrorCode getErrorCode(int statuscode, string CustomMessage = null, params string[] args);
		void AddError(IErrorCode error);
		int ErrorCount { get; }
		IErrorCode[] GetAllErrors(bool flush = true);
		string GetAllErrorsAsJSON(bool flush = true);
	}

	public class ErrorService : IErrorService
	{
		private static IErrorCodeFactory ECF = new bcp.Common.ErrorModels.ErrorCodeFactory();
		private System.Collections.Generic.List<IErrorCode> mErrors;
		public ErrorService()
		{
			mErrors = new System.Collections.Generic.List<IErrorCode>();
		}

		/// <summary>
		/// Create an error and add it to the error collection
		/// </summary>
		/// <param name="statuscode"></param>
		/// <param name="CustomMessage"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public IErrorCode getErrorCode(int statuscode, string CustomMessage = null, params string[] args)
		{
			IErrorCode loError = ECF.getErrorCode(statuscode, CustomMessage, args);
			mErrors.Add(loError);
			return loError;
		}

		public void AddError(IErrorCode error)
		{
			mErrors.Add(error);
		}

		public int ErrorCount { get { return mErrors.Count; } }

		public IErrorCode[] GetAllErrors(bool flush = true)
		{
			IErrorCode[] laErrs = mErrors.ToArray();
			if (flush) { mErrors.Clear(); }
			return laErrs;
		}

		public string GetAllErrorsAsJSON(bool flush = true)
		{
			string lsJson = Utils.ToJSON(mErrors, false);
			if (flush) { mErrors.Clear(); }
			return lsJson;
		}


	}
}
