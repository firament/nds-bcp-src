using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace bcp.Common.ErrorModels
{
	public interface IValidationErrors
	{
		string Message { get; set; }
		List<IValidationError> Errors { get; set; }
		[JsonIgnore]
		bool HasErrors { get; }
		string ToJSON();
	}
	public interface IValidationError
	{
		string FieldName { get; set; }
		string Rule { get; set; }
		string Message { get; set; }
		string ToJSON();
	}

	public class ValidationErrors : IValidationErrors
	{
		public ValidationErrors()
		{
			Errors = new List<IValidationError>();
		}
		public string Message { get; set; }
		public List<IValidationError> Errors { get; set; }
		[JsonIgnore]
		public bool HasErrors { get { return Errors.Count > 0; } }
		public string ToJSON() { return Utils.ToJSON(this, false); }
	}

	public class ValidationError : IValidationError
	{
		public string FieldName { get; set; }
		public string Rule { get; set; }
		public string Message { get; set; }
		public string ToJSON() { return Utils.ToJSON(this, false); }
	}
}
