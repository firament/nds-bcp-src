// Usage
/*
	public ClassName(ILogger<ClassName> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc, bcp.Data.dbsets.BCP_DBContext _ctx)
	: base(_logger, _app_cfg, _user_pay_load, _err_svc, _ctx)
	{
	}
*/

using Microsoft.Extensions.Logging;

using bcp.Common;
using bcp.Common.ErrorModels;
using bcp.Data.dbsets;

public class BCP_BIZ_SVC
{
	internal static UserPayload UPL;
	internal readonly ILogger<BCP_BIZ_SVC> log;
	internal readonly IAppsettings APPCFG;
	internal readonly IErrorService ERRVC;
	internal readonly bcp.Data.dbsets.BCP_DBContext CTX;

	public BCP_BIZ_SVC(
		  ILogger<BCP_BIZ_SVC> _logger
		, IAppsettings _app_cfg
		, UserPayload _user_pay_load
		, IErrorService _err_svc
		, bcp.Data.dbsets.BCP_DBContext _ctx
		)
	{
		log = _logger;
		UPL = _user_pay_load;
		APPCFG = _app_cfg;
		ERRVC = _err_svc;
		CTX = _ctx;
	}


}
