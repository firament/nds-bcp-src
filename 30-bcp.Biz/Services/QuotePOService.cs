#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x

#endregion // Using Declarations

namespace bcp.Biz.Services
{
	public class QuotePOService : BCP_BIZ_SVC
	{
		internal readonly DF.QuotePOFunctions DQPO;
		public QuotePOService(DF.QuotePOFunctions _d_qpo, ILogger<QuotePOService> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc, bcp.Data.dbsets.BCP_DBContext _ctx)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc, _ctx)
		{
			DQPO = _d_qpo;
		}

		public async Task<DM.QuoteRequests> CreateQuote(DM.QuoteRequests quote)
		{
			DM.QuoteRequests loQuote = null;

			// Basic Validation
			if (quote == null)
			{
				log.LogError("Cannot use a null quote. Exiting.");
				return loQuote;
			}
			// force generation if ID
			quote.QuoteId = 0;

			// Set default values
			// quote.QuoteOwner = UPL.Userid;
			quote.ContractNum = Guid.NewGuid().ToString("N");
			quote.Status = (int)CDE.QUOTE_STATUS.NEW;

			// Set timestamps
			DateTime ldtNow = DateTime.UtcNow;
			quote.AddBy = quote.EditBy = UPL.Userid;
			quote.AddOn = quote.EditOn = ldtNow;

			// Add contracts and other things

			loQuote = await DQPO.SaveQuote(quote);
			return loQuote;

		}

		public async Task<List<DM.QuoteRequests>> QuoteRequestList()
		{
			return await DQPO.QuoteRequestList();
		}

		public async Task<DM.QuoteResponses> QuoteResponseNew(int id, int user = 0)
		{
			return await DQPO.QuoteResponseNew(id, user);
		}

		public async Task<DM.QuoteResponses> QuoteResponseSave(DM.QuoteResponses response)
		{
			return await DQPO.QuoteResponseSave(response);
		}

		public async Task<List<DM.QuoteResponses>> QuoteRespondsList()
		{
			return await DQPO.QuoteRespondsList();
		}

	}
}
