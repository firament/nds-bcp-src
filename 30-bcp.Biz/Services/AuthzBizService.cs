#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x
#endregion // Using Declarations

namespace bcp.Biz.Services
{
	public class AuthzBizService : BCP_BIZ_SVC
	{
		internal readonly DF.AuthzFunctions DAF;
		public AuthzBizService(DF.AuthzFunctions d_af, ILogger<AuthzBizService> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc, bcp.Data.dbsets.BCP_DBContext _ctx)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc, _ctx)
		{
			DAF = d_af;
		}

		[Obsolete("", true)]
		public async Task<DM.MaintSessions> WriteSessionLog(DM.MaintSessions slog)
		{
			return await DAF.WriteSessionLog(slog);
		}

		[Obsolete("", true)]
		public async Task<int> AddLoginHistoryEntry(DM.MaintLoginHistory entry)
		{
			return await DAF.AddLoginHistoryEntry(entry);
		}

		public async Task<int> UpdateSessionLog(bool isNewLogin = false)
		{
			return await DAF.UpdateSessionLog(isNewLogin);
		}

		public async Task<DM.MaintSessions> GetCurrentSession()
		{
			return await DAF.GetCurrentSession();
		}

		public async Task<int> CreateUserSession(string UserLogin, string UserPassword)
		{
			log.LogDebug("Debugger hook");
			/*
			In Business service
				check credentials against DB
					Fail 					<== // 400	BadRequestResult
											<== // 404	NotFoundResult
											<== // 409	ConflictResult
				assess user status
					profile incomplete		<== // 401	UnauthorizedResult
				AddLoginHistoryEntry - login
				update userpayload
				update (add?) session entry
					<== // 200	OkResult
			*/
			DM.Users loUser = null;
			CDE.USER_STATUS loStatus;
			DateTime ldtNow = DateTime.UtcNow;
			int liRes = CAEL.DB_NO_RESULT;

			loUser = await DAF.ValidateUserCreds(UserLogin, UserPassword);
			if (loUser == null)
			{
				return liRes;
			}

			loStatus = (CDE.USER_STATUS)loUser.Status;
			switch (loStatus)
			{
				case CDE.USER_STATUS.PENDING_PROFILE:
					liRes = CAEL.USER_PROFILE_PARTIAL;
					break;
				case CDE.USER_STATUS.ACTIVE:
					liRes = CAEL.ALL_OK;
					break;
				default:
					return CAEL.DB_NO_RESULT;
			}

			// ALL OK, proceed with login

			// Update payload
			UPL.Userid = loUser.UserId;
			UPL.Login = loUser.Login;
			UPL.DisplayName = loUser.ScreenName;
			UPL.Role = (CDE.USER_ROLE)loUser.Role;
			UPL.Type = (CDE.USER_TYPE)loUser.Type;
			UPL.Status = CDE.SESSION_STATUS.ACTIVE;
			UPL.IssueTime = ldtNow;
			UPL.AuthTime = ldtNow;
			UPL.LastUsed = ldtNow;

			await DAF.UpdateSessionLog(true);

			return liRes;

		}

		public async Task<DM.UserProfiles> GetUserProfile(int id = 0)
		{
			return await DAF.GetUserProfile(id);
		}

		public async Task<int> SaveUserProfile(DM.UserProfiles profile)
		{
			return await DAF.SaveUserProfile(profile);
		}

	}
}
