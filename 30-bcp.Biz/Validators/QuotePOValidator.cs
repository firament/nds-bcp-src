#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x

#endregion // Using Declarations

namespace bcp.Biz.Validators
{
	public class QuotePOValidator : BCP_BIZ_VAL
	{
		public QuotePOValidator(ILogger<QuotePOValidator> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc)
		{
		}

		public CME.IValidationErrors ValidateQuote(DM.QuoteRequests quote)
		{
			CME.IValidationErrors loValErrs = new CME.ValidationErrors();

			if (quote == null)
			{
				loValErrs.Errors.Add(new CME.ValidationError
				{
					FieldName = "ALL",
					Rule = "Cannot be empty",
					Message = "Cannot process null entity",
				});
				loValErrs.Message = "Unrecoverable error, aborting further processing.";
				log.LogError(loValErrs.ToJSON());
				return loValErrs;
			}

			if (quote.QuoteId == 0)
			// If New
			{
				// // Test for unique
				// if (CTX.LoginIsUnique(user.LoginID) > 1)
				// {
				// 	loValErrs.Errors.Add(new CME.ValidationError
				// 	{
				// 		FieldName = "LoginID",
				// 		Rule = "Cannot be Duplicate",
				// 		Message = "Duplicate LoginID. Cannot process duplicate entity",
				// 	});
				// 	loValErrs.Message = "Unrecoverable error, aborting further processing.";
				// 	log.LogError(loValErrs.ToJson());
				// 	return loValErrs;
				// }

			}
			else
			// If update
			{

			}

			if (string.IsNullOrWhiteSpace(quote.SpecialConditions))
			{
				quote.SpecialConditions = "NO SPECIAL CONDITIONS.";
			}

			return loValErrs;
		}

		public CME.IValidationErrors ValidateResponse(DM.QuoteResponses response)
		{
			CME.IValidationErrors loValErrs = new CME.ValidationErrors();
			return loValErrs;
		}


	}
}
