#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x
#endregion // Using Declarations

namespace bcp.Biz.Validators
{
	public class AuthzBizValidators : BCP_BIZ_VAL
	{
		DF.AuthzFunctions DAF;
		public AuthzBizValidators(ILogger<AuthzBizValidators> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc, DF.AuthzFunctions d_af)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc)
		{
			DAF = d_af;
		}

		public async Task<(CME.IValidationErrors, DM.MaintSessions)> ValidateSession(string token, DM.MaintSessions sessionlog)
		{
			DM.MaintSessions loMS = null; // = CTX.GetSessionByToken(token);
			CC.UserPayload loUPL = null;
			CME.IValidationErrors loValErrs = new CME.ValidationErrors() { Message = "Validation Failed. Unprocessable" };
			CME.IErrorCode loErr;

			if (token == null)
			{
				loErr = ERRVC.getErrorCode(CAEL.SESSIONLOG_MISSING);
				loValErrs.Message = $"ErrorID = {loErr.ErrorTrackID}, {loErr.ErrorMesg}";
				loValErrs.Errors.Add(new CME.ValidationError { FieldName = "token", Rule = "Required" });
				log.LogError("Empty session log, Unprocessable", loErr.ToJSON());
				return (loValErrs, loMS);
			}
			if (sessionlog == null)
			{ loMS = await DAF.GetSessionByToken(token); }
			else
			{ loMS = sessionlog; }

			if (loMS == null)
			{
				loErr = ERRVC.getErrorCode(CAEL.SESSIONLOG_MISSING);
				loValErrs.Message = $"ErrorID = {loErr.ErrorTrackID}, {loErr.ErrorMesg}";
				loValErrs.Errors.Add(new CME.ValidationError { });
				log.LogError("Missing session log, Unprocessable", loErr.ToJSON());
				return (loValErrs, loMS);
			}

			// Match token, userid, role, type, status & times (issue, auth, last used)
			// loUPL = CC.UserPayload.FromJSON(loMS.Payload);
			loUPL = loMS.User_Payload;
			if (
				   loUPL.Token != loMS.Token
				&& (int)loUPL.Status != loMS.Status
			// && loUPL.Role != loMS.Role
			// && loUPL.Type != loMS.Type
			// && loUPL.IssueTime != loMS.IssueTime
			// && loUPL.AuthTime != loMS.AuthTime
			// && loUPL.LastUsed != loMS.LastUsed
			)
			{
				loErr = ERRVC.getErrorCode(CAEL.SESSIONLOG_MISSING);
				loValErrs.Message = $"ErrorID = {loErr.ErrorTrackID}, {loErr.ErrorMesg}";
				loValErrs.Errors.Add(new CME.ValidationError { });
				log.LogError("Missing session log, Unprocessable", loErr.ToJSON());
				return (loValErrs, loMS);
			}

			// For debugging
			if (loMS.Userid != loUPL.Userid || loMS.UserLogin != loUPL.Login)
			{
				log.LogWarning("ATTENTON: ID Mismatch. Inspect logic.");
			}

			loValErrs.Message = "OK";
			UPL.Token = loUPL.Token;
			UPL.DeviceSig = loUPL.DeviceSig;
			UPL.Userid = loUPL.Userid;
			UPL.Login = loUPL.Login;
			UPL.DisplayName = loUPL.DisplayName;
			UPL.Role = loUPL.Role;
			UPL.Type = loUPL.Type;
			UPL.Status = loUPL.Status;
			UPL.IssueTime = loUPL.IssueTime;
			UPL.AuthTime = loUPL.AuthTime;
			UPL.LastUsed = loUPL.LastUsed;
			UPL.Msid = loMS.Msid;   // loUPL.Msid; ID will not be availaible for newly created session.
			if (!UPL.Equals(loMS.User_Payload)) { log.LogError("Payload Mismatch, Review code."); }
			return (loValErrs, loMS);
		}


	}
}
