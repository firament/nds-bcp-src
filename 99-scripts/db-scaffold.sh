#! /bin/bash

# Clear earlier generations
echo "Cleaning up older files now";
rm -vrf 60-bcp.Data-Scaffold/dbsets/*.cs
pushd 60-bcp.Data-Scaffold;

# Using MySql.Data.EntityFrameworkCore - PREFERRED
echo "Scaffolding now.."
dotnet ef dbcontext scaffold \
	"server=127.0.0.1;port=8804;database=BCP_DB;user=bcp-dev-rw;password=cfea457b656147e3" \
	MySql.Data.EntityFrameworkCore \
	-v -f \
	-d \
	-o dbsets \
	-c BCP_DBContext \
	--framework netcoreapp2.2 \
	2>&1 | tee ../../docs/01-Notes/downloads/run-logs/db-scaffold.log

popd;

echo "Syncing generated files from 60-bcp.Data-Scaffold/dbsets/ -> 40-bcp.Data/dbsets/";
rsync -vhr 60-bcp.Data-Scaffold/dbsets/*.cs 40-bcp.Data/dbsets
# sync files to constituent project
