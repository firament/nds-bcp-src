#! /bin/bash

# Clean
rm -vfr x-out/BCP-Website/*

# Publish
DAY_TAG=$(date +"%Y%m%d-%s");
OUT_DIR="x-out/BCP-Website/R-${DAY_TAG}";
echo "Building to DIR ${OUT_DIR}";

mkdir -vp ${OUT_DIR};
dotnet publish \
	-v n \
	-f netcoreapp3.1 \
	-c Release \
	-o ${PWD}/${OUT_DIR} \
	20-bcp.web/bcp.web.csproj \
	2>&1 | tee ${OUT_DIR}-pub.log

# Scrub
rm -vfr ${OUT_DIR}/wwwroot/nlog-output;
rm -vf ${OUT_DIR}/appsettings.Development.json;
rm -vf ${OUT_DIR}/NLog.Development.config;

# # Run Test
# pushd ${OUT_DIR};
# dotnet bcp.web.dll --urls "http://*:5020"
# # dotnet bcp.web.dll --urls="http://*:5020;https://*:5030"
# popd;

# Pack
pushd ${OUT_DIR}/../
tar cvzf gpmweb-${DAY_TAG}.tar.gz R-${DAY_TAG}
echo "Packed file:"
echo "bcp.web-${DAY_TAG}.tar.gz";
echo "${PWD}/bcp.web-${DAY_TAG}.tar.gz";
popd
