// Usage
/*
	public ClassName(ILogger<ClassName> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc)
	: base(_logger, _app_cfg, _user_pay_load, _err_svc)
	{
	}
*/

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using bcp.Common;
using bcp.Common.ErrorModels;

public class BCP_Controller : Controller
{
	internal UserPayload UPL;
	internal readonly ILogger<BCP_Controller> log;
	internal readonly IAppsettings APPCFG;
	internal readonly IErrorService ERRVC;

	public BCP_Controller(
		  ILogger<BCP_Controller> _logger
		, IAppsettings _app_cfg
		, UserPayload _user_pay_load
		, IErrorService _err_svc
		)
	{
		log = _logger;
		UPL = _user_pay_load;
		APPCFG = _app_cfg;
		ERRVC = _err_svc;
	}

}
