#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 20-x
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BB = bcp.Biz;
using BI = bcp.Biz.Interfaces;
using BS = bcp.Biz.Services;
using BV = bcp.Biz.Validators;
// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x

#endregion // Using Declarations
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using System.Security.Principal;

namespace bcp.web.Controllers
{
	/// <summary>
	/// 
	/// </summary>
	public class SessionController : BCP_Controller
	{
		private bcp.Biz.Validators.AuthzBizValidators VAUTH;
		private BS.AuthzBizService BAS;
		public SessionController(ILogger<SessionController> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc, BS.AuthzBizService b_as, bcp.Biz.Validators.AuthzBizValidators v_auth)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc)
		{
			BAS = b_as;
			VAUTH = v_auth;
		}



		/// <summary>
		/// The login display form
		/// </summary>
		/// <returns></returns>
		[AllowAnonymous]
		public IActionResult Login()
		{
			// log.LogDebug("==> Login");
			return View();
		}

		/// <summary>
		/// Validate and Veriy credentials posted by Login
		/// </summary>
		/// <returns></returns>
		[AllowAnonymous]
		public async Task<IActionResult> SignIn([FromForm]string UserLogin, [FromForm]string UserPWD)
		{
			log.LogDebug("==> SignIn");
			DateTime ldtNow = System.DateTime.UtcNow;

			// Signout user if already logged in
			if (User.Identity.IsAuthenticated)
			{
				log.LogInformation("User already logged in, clearing session before proceeding");

				// Add log history entry
				int liUserID = int.Parse(User.Claims.First(c => c.Type == Consts.KEYS.USERID_KEY)?.Value ?? "0");
				// TODO: Update session record on logout.
				// await BAS.AddLoginHistoryEntry(new DM.MaintLoginHistory
				// {
				// 	UserId = liUserID,
				// 	UserLogin = User.Identity.Name,
				// 	Status = (int)CDE.LOG_ENTRY_TYPE.LOGOUT,
				// 	AddOn = ldtNow,
				// });
				// await HttpContext.SignOutAsync(APPCFG.SessionOpts.AuthSchemeName);
			}

			// Continue with hashed password only from here onwards
			string lsPwdHash = CU.GetHashedPWD(UserPWD);
			lsPwdHash = UserPWD;    // TODO: remove after passwords are hashed in the system.

			int liRes = await AuthenticateUser(UserLogin, lsPwdHash);
			log.LogInformation("IsAuthenticated = {0}, Expected TRUE", this.HttpContext.User.Identity.IsAuthenticated);
			switch (liRes)
			{
				case CAEL.ALL_OK:
					break;
				case CAEL.USER_PROFILE_PARTIAL:
					// redirect to profile page
					log.LogInformation("Profile is missing or Incomplete, redirecting to Profie page");
					return RedirectToAction("UserProfile", "Session");
					// break;
				case CAEL.FEILD_MISSING_EMPTY_BAD:
				case CAEL.DB_NO_RESULT:
				case CAEL.DB_INSERT_ERROR:
				default:
					// return fail message
					TempData[Consts.KEYS.ERROR_VIEWBAG] = $"Login Failed with code {liRes}.";
					log.LogInformation("Failed login attempt");
					return RedirectToAction("Login", "Session");
					// break;
			}

			// For now, redirect all to single langin page.
			log.LogInformation("Signin OK. Redirecting to DefaultLanding page.");

			// return RedirectToAction("DefaultLanding", "Home");
			return RedirectToRoute(new { controller = "Home", action = "DefaultLanding" });


			// (StatusCodeResult Status, Models.UserPayload UserData, System.Threading.Tasks.Task AuthResult) loAuthID;
			// loAuthID = AuthenticateUser(UserID, UserPWD);
			// if (!(loAuthID.Status is OkResult))
			// {
			// 	// log.LogWarning("Authentication failed. See Prior entries for details.");
			// 	TempData[Consts.KEYS.ERROR_VIEWBAG] = "Login Failed.";
			// 	return RedirectToAction("Login");
			// }

			// switch (loAuthID.UserData.Role)
			// {
			// 	case Consts.USER_ROLES.USER:
			// 		// log.LogInformation("User {0}/{1} Authenticated to role {2}. Redirecting to User landing page."
			// 		// 				, loAuthID.UserData.Userid
			// 		// 				, UserID
			// 		// 				, loAuthID.UserData.Role
			// 		// 				);
			// 		return RedirectToAction("IndexUser", "Home");
			// 	case Consts.USER_ROLES.ADMIN:
			// 		// log.LogInformation("User {0}/{1} Authenticated to role {2}. Redirecting to Admin landing page."
			// 		// 				, loAuthID.UserData.Userid
			// 		// 				, UserID
			// 		// 				, loAuthID.UserData.Role
			// 		// 				);
			// 		return RedirectToAction("IndexAdmin", "Home");
			// 	default:
			// 		break;
			// }

			// // log.LogInformation("User {0}/{1} Authenticated to role {2}. Role is Unknown, redirecting back to Login Page."
			// // 						, loAuthID.UserData.Userid
			// // 						, UserID
			// // 						, loAuthID.UserData.Role
			// // 						);
			// TempData[Consts.KEYS.ERROR_VIEWBAG] = "Unknown Role assigned. Contact your Administrator.";
			// return RedirectToAction("Login");
		}

		[AllowAnonymous]
		[HttpGet()]
		public IActionResult NewUser()
		{
			return View();
		}

		[AllowAnonymous]
		[HttpPost("NewUserConfirm")]
		public async Task<ActionResult> NewUserConfirm(CM.NewUserForm userform)
		{
			return View();
		}

		[AllowAnonymous]
		[HttpPost("RegisterUser")]
		public async Task<ActionResult> RegisterUser(CM.NewUserForm userform)
		{
			return View();
		}

		[AllowAnonymous]
		[HttpGet("UserProfile")]
		public async Task<ActionResult> UserProfile(int id = 0)
		{
			DM.UserProfiles loProfile = null;
			int liUserID = id == 0 ? UPL.Userid : id;
			loProfile = await BAS.GetUserProfile(liUserID);
			return View(loProfile);
		}

		[AllowAnonymous]
		[HttpPost("SaveUserProfile")]
		public async Task<ActionResult> SaveUserProfile(DM.UserProfiles profile)
		{
			await BAS.SaveUserProfile(profile);
			return RedirectToAction("DefaultLanding", "Home");
		}



		/// <summary>
		/// Logout the current user, and redirect to login form.
		/// </summary>
		/// <returns></returns>
		[AllowAnonymous]
		public IActionResult Logout()
		{
			// log.LogDebug("==> Logout");
			HttpContext.SignOutAsync(APPCFG.SessionOpts.AuthSchemeName);
			return RedirectToAction("Login");
		}



		// TODO: Move to bcp.web.Infra.Authenticator
		// 	Once Di for transient service or to a static class is resolved.
		private async Task<int> AuthenticateUser(string UserLogin, string UserPassword)
		{
			log.LogDebug("Debugger hook");
			if ((string.IsNullOrWhiteSpace(UserLogin)) || (string.IsNullOrWhiteSpace(UserPassword)))
			{
				// Fail the request, write details in the log
				log.LogError("Unable to authenticate user with given data");
				return CAEL.FEILD_MISSING_EMPTY_BAD;
			}

			int liRes = await BAS.CreateUserSession(UserLogin, UserPassword);
			switch (liRes)
			{
				case CAEL.USER_PROFILE_PARTIAL:
				case CAEL.DB_NO_RESULT:
					return liRes;
				default:
					break;
			}

			//##	create identity object
			// set auth options
			var loAuthProperties = new AuthenticationProperties
			{
				AllowRefresh = true,
				IsPersistent = false,
			};
			// build set of claims
			List<Claim> lstClaims = new List<Claim>
				{
				new Claim(ClaimTypes.Name, UPL.DisplayName),
				new Claim(ClaimTypes.Role, UPL.Role.ToString()),
				new Claim(Consts.KEYS.USER_TYPE_KEY, UPL.Type.ToString()),
				new Claim(UPL.Role.ToString(), "true", ClaimValueTypes.Boolean),
				new Claim(UPL.Type.ToString(), "true", ClaimValueTypes.Boolean),
				new Claim(ClaimTypes.SerialNumber, UPL.Token),
				new Claim(Consts.KEYS.USERID_KEY, UPL.Userid.ToString(), ClaimValueTypes.Integer),
				};
			// prepare identity for auth
			ClaimsIdentity loClaimsIdentity = new ClaimsIdentity(
					  lstClaims
					, APPCFG.SessionOpts.AuthSchemeName
					);

			//##	Authenticate user
			// System.Threading.Tasks.Task ltResult = HttpContext.SignInAsync(
			// 	  APPCFG.SessionOpts.AuthSchemeName
			// 	, new ClaimsPrincipal(loClaimsIdentity)
			// 	, loAuthProperties
			// 	);

			await HttpContext.SignInAsync(
							  APPCFG.SessionOpts.AuthSchemeName
							, new ClaimsPrincipal(loClaimsIdentity)
							, loAuthProperties
			);

			log.LogDebug("Authenticated status: {0}", HttpContext.User.Identity.IsAuthenticated);


			// TODO: Capture true result of signin
			// if (!ltResult.IsCompletedSuccessfully)
			// {
			// 	log.LogWarning("Identity Signon failed. Status = {0} Inspect."
			// 			, ltResult.Status
			// 			);
			// 	return CAEL.DB_INSERT_ERROR;
			// }

			return CAEL.ALL_OK;

		}
	}
}
