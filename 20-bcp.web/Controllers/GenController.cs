#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 20-x
using Microsoft.AspNetCore.Mvc;
using BB = bcp.Biz;
using BI = bcp.Biz.Interfaces;
using BS = bcp.Biz.Services;
using BV = bcp.Biz.Validators;
// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x

#endregion // Using Declarations

namespace bcp.web.Controllers
{
	public class GenController : BCP_Controller
	{
		public GenController(ILogger<GenController> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc)
		{
		}
	}
}
// ClassName
