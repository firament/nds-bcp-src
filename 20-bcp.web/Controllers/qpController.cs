#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 20-x
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BB = bcp.Biz;
using BI = bcp.Biz.Interfaces;
using BS = bcp.Biz.Services;
using BV = bcp.Biz.Validators;
// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x

#endregion // Using Declarations


namespace bcp.web.Controllers
{
	public class qpController : BCP_Controller
	{
		internal readonly BV.QuotePOValidator VQPO;
		internal readonly BS.QuotePOService BQPO;
		internal readonly DF.QuotePOFunctions DQPO;

		public qpController(BV.QuotePOValidator v_qpo, BS.QuotePOService b_qpo, DF.QuotePOFunctions d_qpo, ILogger<qpController> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc)
		: base(_logger, _app_cfg, _user_pay_load, _err_svc)
		{
			VQPO = v_qpo;
			BQPO = b_qpo;
			DQPO = d_qpo;
		}

		[AllowAnonymous]
		[HttpGet()]
		public async Task<ActionResult> Index()
		{
			// Add condition to filter by quote owner or transporter
			List<DM.QuoteRequests> lstRequests = await BQPO.QuoteRequestList();
			return View(lstRequests);
		}


		[AllowAnonymous]
		[HttpGet()]
		public async Task<ActionResult> QuoteNew(int id)
		{
			DM.QuoteRequests loQuote = new DM.QuoteRequests
			{
				QuoteOwner = 1, // UPL.Userid,
				Transporters = await DQPO.GetTransporters(UPL.Userid)
			};
			return View(loQuote);

			// Test code for use in view
			// foreach (DM.Users vTrt in loQuote.Transporters)
			// {
			// 	var lsOpt = $"{vTrt.UserId} = {vTrt.FullName} = {vTrt.Status}";
			// }
			// CDE.DD_CATEGORIES.TRUCK_TYPES
			// var x = loQuote.Destination
		}

		[AllowAnonymous]
		[HttpPost("QuoteSave")]
		public async Task<ActionResult> QuoteSave(DM.QuoteRequests quote)
		{
			CME.IValidationErrors loErrs = VQPO.ValidateQuote(quote);
			if (loErrs.HasErrors)
			{
				return new BadRequestObjectResult(loErrs);
			}

			if (quote.QuoteId == 0)
			{
				await BQPO.CreateQuote(quote);
			}
			return RedirectToAction("Index", "qp");
		}

		[AllowAnonymous]
		[HttpGet("QuoteRespond")]
		public async Task<ActionResult> QuoteRespond(int id)		{
			DM.QuoteResponses loResponse = null;
			loResponse = await BQPO.QuoteResponseNew(id);
			return View(loResponse);
		}

		[AllowAnonymous]
		[HttpPost("ResponseSave")]
		public async Task<ActionResult> ResponseSave(DM.QuoteResponses response)
		{
			log.LogDebug("Inclusions are {0}", Request.Form["QuoteResponseIncludes.Inclusions"].ToList());
			log.LogDebug("Debugger hook - inspect response");
			CME.IValidationErrors loErrs = VQPO.ValidateResponse(response);
			if (loErrs.HasErrors)
			{
				return new BadRequestObjectResult(loErrs);
			}

			await BQPO.QuoteResponseSave(response);
			return RedirectToAction("Index", "qp");
		}

		[AllowAnonymous]
		[HttpPost("ResponseList")]
		public async Task<ActionResult> ResponseList(int id)
		{
			List<DM.QuoteResponses> lstResponds = await BQPO.QuoteRespondsList();
			return View(lstResponds);
			
		}

	}
}
