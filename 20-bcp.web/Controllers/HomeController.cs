﻿#region    // Using Declarations
// Common
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

// For 20-x
using Microsoft.AspNetCore.Mvc;
using BB = bcp.Biz;
using BI = bcp.Biz.Interfaces;
using BS = bcp.Biz.Services;
using BV = bcp.Biz.Validators;
// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x

#endregion // Using Declarations
using System.Diagnostics;
using bcp.web.Models;
using Microsoft.AspNetCore.Authorization;

namespace bcp.web.Controllers
{
	public class HomeController : Controller
	{
		ILogger<HomeController> log;
		private bcp.Data.Views.DD_Lookup DDHelp;
		// private bcp.Biz.Validators.AuthzBizValidators VAUTH;
		// private BS.AuthzBizService BAS;
		public HomeController(ILogger<HomeController> logger, bcp.Data.Views.DD_Lookup dd_help)
		{
			log = logger;
			DDHelp = dd_help;
		}

		// public HomeController(ILogger<HomeController> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc, BS.AuthzBizService b_as, bcp.Biz.Validators.AuthzBizValidators v_auth)
		// : base(_logger, _app_cfg, _user_pay_load, _err_svc)
		// {
		// 	BAS = b_as;
		// 	VAUTH = v_auth;
		// }

		[AllowAnonymous]
		public IActionResult Index()
		{
			return View();
		}

		[AllowAnonymous]
		public IActionResult Privacy()
		{
			return View();
		}

		[AllowAnonymous]
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		[AllowAnonymous]
		public ActionResult DefaultLanding(){
			return View();
		}
	}
}
