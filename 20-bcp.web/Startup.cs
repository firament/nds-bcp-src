using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
// using MySql.Data.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

#region 
// using System.IO;
// For 20-x
using BB = bcp.Biz;
using BI = bcp.Biz.Interfaces;
using BS = bcp.Biz.Services;
using BV = bcp.Biz.Validators;
// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;
#endregion
using Microsoft.AspNetCore.Http;

namespace bcp.web
{
	public class Startup
	{
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			bool lbIsDevEnv = (Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") == Microsoft.Extensions.Hosting.Environments.Development);
			string lsAppConfig =
						lbIsDevEnv
						? "appsettings.Development.json"
						: "appsettings.json"
						;
			string lsConnStrRW = Configuration.GetSection("AppSettings:DBConnectionStringRW").Value;
			string lsConnStrRO = Configuration.GetSection("AppSettings:DBConnectionStringRO").Value;
			string lsAppCode = Configuration.GetSection("AppSettings:AppCode").Value;
			string lsAppSettingVersion = Configuration.GetSection("AppSettings:AppSettingVersion").Value;
			int liAppSettingVersion = 0;
			if (!int.TryParse(lsAppSettingVersion, out liAppSettingVersion))
			{
				log.Fatal("Version number not in proper format");
				throw new ArgumentException("Version number not in proper format");
			}
			// For seeding only - BEGIN
			if (lbIsDevEnv)
			{
				// bcp.Data.Functions.CommonFunctions.AddSampleSettings(lsConnStrRW, lsAppCode, liAppSettingVersion);
				// string lsEnums = bcp.Data.Functions.WF.WF_1.getDDLookupsAsEnums(lsConnStrRO);
				// log.Debug($"\n{lsEnums}\n");
			}
			// For seeding only - BEGIN

			CC.IAppsettings loAppCfg = bcp.Data.Functions.CommonFunctions.GetAppSettings(lsConnStrRO, lsAppCode, liAppSettingVersion) as CC.Appsettings;
			// Hoist dynamic data
			loAppCfg.DynamicData.DBConnRO = lsConnStrRO;
			loAppCfg.DynamicData.DBConnRW = lsConnStrRW;

			#region DB Contexts
			//
			// Activate Database connectivity
			//
			log.Info("DbContext: Hook into DI");
			services.AddDbContext<bcp.Data.dbsets.BCP_DBContext>(options => options.UseMySql(lsConnStrRW));

			// TODO: Inspect and remove
			// services.AddDbContext<bcp.Data.dbsets.BCP_DBContext>(
			// 	options => options.UseMySql(lsConnStrRW,
			// 		mysqlOptions =>
			// 		{
			// 			mysqlOptions
			// 				.CharSetBehavior(CharSetBehavior.AppendToAllColumns)
			// 				.EnableRetryOnFailure(3)
			// 				.SetSqlModeOnOpen();
			// 		}
			// ));
			#endregion DB Contexts

			#region Configure Cookie-based Authentication
			log.Info("Authentication: Configure Cookie-based Authentication");
			services
				.AddAuthentication(loAppCfg.SessionOpts.AuthSchemeName)
				.AddCookie(
					  loAppCfg.SessionOpts.AuthSchemeName
					, options =>
						{
							options.SlidingExpiration = true;
							options.LoginPath = new PathString(loAppCfg.SessionOpts.SignOnPath);
							options.LogoutPath = new PathString(loAppCfg.SessionOpts.SignOutPath);
							// options.AccessDeniedPath = new PathString(loAppCfg.SessionOpts.AccessDenyPath);
							options.AccessDeniedPath = new PathString(loAppCfg.SessionOpts.SignOnPath);
							options.ReturnUrlParameter = loAppCfg.SessionOpts.ReturnURL_TagName;  // get from loAppSetting
							options.ExpireTimeSpan = CU.TimeSpanFromMinutes(loAppCfg.SessionOpts.SessionTimeoutMinutes);
							// Auth Cookie options
							options.Cookie.Name = loAppCfg.SessionOpts.AuthCookieName;
							options.Cookie.Path = "/";
							options.Cookie.Domain = loAppCfg.SessionOpts.AuthCookieDomain;
							options.Cookie.SameSite = SameSiteMode.None;
							options.Cookie.SecurePolicy = CookieSecurePolicy.None;
							options.Cookie.HttpOnly = true;
							// options.Cookie.IsEssential = true;
						}
				)
				;
			services
				.Configure<CookiePolicyOptions>(options =>
				{
					// This lambda determines whether user consent for non-essential cookies is needed for a given request.
					options.CheckConsentNeeded = context => false;
					options.MinimumSameSitePolicy = SameSiteMode.None;
					options.HttpOnly = Microsoft.AspNetCore.CookiePolicy.HttpOnlyPolicy.None;
					options.Secure = CookieSecurePolicy.None;
				});

			#endregion Configure Cookie-based Authentication

			services
				.AddControllersWithViews(options =>
					{
						options.Filters.Add(typeof(bcp.web.Infra.Filters.GateActionFilter));
						// options.Filters.Add(typeof(bcp.web.Infra.Filters.GateActionFilterAsync));
					})
				.AddNewtonsoftJson(options => options.UseMemberCasing())
				;

			// Singleton Services
			services.AddSingleton<bcp.Common.IAppsettings>(loAppCfg);
			services.AddSingleton<bcp.Common.Constants.ErrorCodeList>();
			services.AddSingleton<bcp.Data.Views.DD_Lookup>();

			// Scoped Services
			services.AddScoped<bcp.Common.ErrorModels.IErrorCodeFactory, bcp.Common.ErrorModels.ErrorCodeFactory>();
			services.AddScoped<bcp.Common.ErrorModels.IErrorService, bcp.Common.ErrorModels.ErrorService>();
			services.AddScoped<bcp.Common.UserPayload>();

			// Validator Services
			services.AddScoped<bcp.Biz.Validators.QuotePOValidator>();
			// Pending review, to keep or remove
			services.AddScoped<bcp.Biz.Validators.AdminBizValidators>();
			services.AddScoped<bcp.Biz.Validators.AnchorClientBizValidators>();
			services.AddScoped<bcp.Biz.Validators.AuthzBizValidators>();
			services.AddScoped<bcp.Biz.Validators.CommonBizValidators>();
			services.AddScoped<bcp.Biz.Validators.EmailBizValidators>();
			services.AddScoped<bcp.Biz.Validators.LenderBizValidators>();
			services.AddScoped<bcp.Biz.Validators.MessagingBizValidators>();
			services.AddScoped<bcp.Biz.Validators.PlatformBizValidators>();

			// Business Services
			services.AddScoped<bcp.Biz.Services.QuotePOService>();
			// Pending review, to keep or remove
			services.AddScoped<bcp.Biz.Services.AdminBizService>();
			services.AddScoped<bcp.Biz.Services.AnchorClientBizService>();
			services.AddScoped<bcp.Biz.Services.AuthzBizService>();
			services.AddScoped<bcp.Biz.Services.CommonBizService>();
			services.AddScoped<bcp.Biz.Services.EmailBizService>();
			services.AddScoped<bcp.Biz.Services.LenderBizService>();
			services.AddScoped<bcp.Biz.Services.MessagingBizService>();
			services.AddScoped<bcp.Biz.Services.PlatformBizService>();

			// Data Services
			services.AddScoped<bcp.Data.Functions.QuotePOFunctions>();
			services.AddScoped<bcp.Data.Functions.AuthzFunctions>();

		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				// app.UseHsts();
			}
			// app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
