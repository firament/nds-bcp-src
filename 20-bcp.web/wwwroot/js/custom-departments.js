// Default selection to drive validation
var Dept_None = [
	{ Catg: 0, ddCode: 0, DispText: "Select a Department..." },
]

// Clinical Departments, ordered by usage
var Departments = [
	{ Catg: 3, ddCode: 101, DispText: "Cardiology" },
	{ Catg: 3, ddCode: 102, DispText: "Neurology" },
	{ Catg: 3, ddCode: 103, DispText: "Neurosurgery" },
	{ Catg: 3, ddCode: 104, DispText: "General Medicine" },
	{ Catg: 3, ddCode: 105, DispText: "Orthopaedics" },
	{ Catg: 3, ddCode: 106, DispText: "Paediatrics" },
	{ Catg: 3, ddCode: 107, DispText: "Obstetrics & Gynaecology" },
	{ Catg: 3, ddCode: 108, DispText: "Ophthalmology" },
	{ Catg: 3, ddCode: 109, DispText: "Dentistry" },
	{ Catg: 3, ddCode: 110, DispText: "ENT" },
	{ Catg: 3, ddCode: 111, DispText: "General Surgery" },
	{ Catg: 3, ddCode: 112, DispText: "Dermatology" },
	{ Catg: 3, ddCode: 113, DispText: "Urology" },
	{ Catg: 3, ddCode: 114, DispText: "Endocrinology" },
	{ Catg: 3, ddCode: 115, DispText: "Wellness Clinic" },
	{ Catg: 3, ddCode: 116, DispText: "Psychiatry" },
	{ Catg: 3, ddCode: 117, DispText: "Lab" },
	{ Catg: 3, ddCode: 118, DispText: "Radiology" },
	{ Catg: 3, ddCode: 119, DispText: "Physiotheapy" },
	{ Catg: 3, ddCode: 120, DispText: "PAC Pre Anesthtic Check" },
	{ Catg: 3, ddCode: 121, DispText: "Sai Counselling" },
	{ Catg: 3, ddCode: 122, DispText: "Other" },
	{ Catg: 3, ddCode: 123, DispText: "Cardiac Surgery" },
]

// Vendor specific departments, ordered by usage
var DEPT_V_VENDOR = [
	{ Catg: 19, ddCode: 201, DispText: "Canteen" },
	{ Catg: 19, ddCode: 202, DispText: "Bakery" },
	{ Catg: 19, ddCode: 203, DispText: "Mailing Center" },
	{ Catg: 19, ddCode: 206, DispText: "Medical Stores" },
	{ Catg: 19, ddCode: 207, DispText: "Central Stores" },
	{ Catg: 19, ddCode: 209, DispText: "Eng-Civil" },
	{ Catg: 19, ddCode: 210, DispText: "Eng-Electrical" },
	{ Catg: 19, ddCode: 211, DispText: "Eng-HVAC" },
	{ Catg: 19, ddCode: 212, DispText: "Eng-Medical Gases" },
	{ Catg: 19, ddCode: 213, DispText: "Biomedical" },
	{ Catg: 19, ddCode: 214, DispText: "Dietary" },
	{ Catg: 19, ddCode: 215, DispText: "Sevadal Office" },
]

// Staff Visitor specific departments, ordered by usage
var DEPT_V_STAFF = [
	{ Catg: 20, ddCode: 301, DispText: "Director's Office" },
	{ Catg: 20, ddCode: 302, DispText: "HR" },
	{ Catg: 20, ddCode: 303, DispText: "Academic" },
	{ Catg: 20, ddCode: 304, DispText: "Finance" },
	{ Catg: 20, ddCode: 305, DispText: "College of Nursing" },
	{ Catg: 20, ddCode: 306, DispText: "Medical Stores" },
	{ Catg: 20, ddCode: 307, DispText: "Central Stores" },
	{ Catg: 20, ddCode: 308, DispText: "Counselling" },
	{ Catg: 20, ddCode: 309, DispText: "Eng-Civil" },
	{ Catg: 20, ddCode: 310, DispText: "Eng-Electrical" },
	{ Catg: 20, ddCode: 311, DispText: "Eng-HVAC" },
	{ Catg: 20, ddCode: 312, DispText: "Eng-Medical Gases" },
	{ Catg: 20, ddCode: 313, DispText: "Biomedical" },
	{ Catg: 20, ddCode: 314, DispText: "Dietary" },
	{ Catg: 20, ddCode: 315, DispText: "Sevadal Office" },
]

// Clinical departments, ordered by name
var DEPTS_V = [
	{ Catg: 3, ddCode: 123, DispText: "Cardiac Surgery" },
	{ Catg: 3, ddCode: 101, DispText: "Cardiology" },
	{ Catg: 3, ddCode: 109, DispText: "Dentistry" },
	{ Catg: 3, ddCode: 112, DispText: "Dermatology" },
	{ Catg: 3, ddCode: 114, DispText: "Endocrinology" },
	{ Catg: 3, ddCode: 110, DispText: "ENT" },
	{ Catg: 3, ddCode: 104, DispText: "General Medicine" },
	{ Catg: 3, ddCode: 111, DispText: "General Surgery" },
	{ Catg: 3, ddCode: 117, DispText: "Lab" },
	{ Catg: 3, ddCode: 102, DispText: "Neurology" },
	{ Catg: 3, ddCode: 103, DispText: "Neurosurgery" },
	{ Catg: 3, ddCode: 107, DispText: "Obstetrics & Gynaecology" },
	{ Catg: 3, ddCode: 108, DispText: "Ophthalmology" },
	{ Catg: 3, ddCode: 105, DispText: "Orthopaedics" },
	{ Catg: 3, ddCode: 120, DispText: "PAC Pre Anesthtic Check" },
	{ Catg: 3, ddCode: 106, DispText: "Paediatrics" },
	{ Catg: 3, ddCode: 119, DispText: "Physiotheapy" },
	{ Catg: 3, ddCode: 116, DispText: "Psychiatry" },
	{ Catg: 3, ddCode: 118, DispText: "Radiology" },
	{ Catg: 3, ddCode: 121, DispText: "Sai Counselling" },
	{ Catg: 3, ddCode: 113, DispText: "Urology" },
	{ Catg: 3, ddCode: 115, DispText: "Wellness Clinic" },
	{ Catg: 3, ddCode: 122, DispText: "Other" },
]

// .cstm-custom-department-driver
// .cstm-custom-departments
function updateDepartmentList () {
	var liType = event.currentTarget.value;
	var lstTarget = $("#Department");	// .cstm-custom-departments
	applyDepartmentListUpdate();
}

// Split into functions to allow direct interface
function applyDepartmentListUpdate (piType, pstTarget) {

	// Clear all departments first
	$(pstTarget).find("option").remove();

	// Apply defaults
	$(Dept_None).each(function () {
		$(pstTarget).append($("<option />").html(this.DispText).val(this.ddCode));
	});

	// Apply custom list
	switch (piType)
	{
		case "210":
			$(DEPT_V_VENDOR).each(function () {
				$(pstTarget).append($("<option />").html(this.DispText).val(this.ddCode));
			});
			$(DEPTS_V).each(function () {
				$(pstTarget).append($("<option />").html(this.DispText).val(this.ddCode));
			});
			break;
		case "220":
			$(DEPT_V_STAFF).each(function () {
				$(pstTarget).append($("<option />").html(this.DispText).val(this.ddCode));
			});
			$(DEPTS_V).each(function () {
				$(pstTarget).append($("<option />").html(this.DispText).val(this.ddCode));
			});
			break;
		default:
			$(Departments).each(function () {
				$(pstTarget).append($("<option />").html(this.DispText).val(this.ddCode));
			});
			break;
	}
	
}

