function doDocumentReadyActions () {

	// Update any custom department lists, before custom-selects are run.
	if ($(".cstm-custom-department-driver").length == 1 && $(".cstm-custom-departments").length == 1)
	{
		applyDepartmentListUpdate(
			$(".cstm-custom-department-driver").attr("data-value-current")
			, $(".cstm-custom-departments")
		);
	}

	// Silently apply current values to all selects in editors
	$(".cstm-select-bound").each(function (idx, elem) {
		if ($(elem).attr("data-value-current"))
		{
			$(elem).val($(elem).attr("data-value-current"));
		} else
		{
			console.info("Select:" + elem.name + " => Attribute 'data-value-current' not defined, skipping.");
		}
	})

	// Disable unusable options
	$(".cstm-option-disabled").each(function (idx, elem) {
		$(elem).attr("disabled", "disabled");
	})
	// $(".cstm-option-disabled").each(function (idx, elem) { $(elem).attr("disabled", "disabled"); })

}
