#region    // Using Declarations
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Filters;
// For 20-x
using Microsoft.AspNetCore.Mvc;
using BB = bcp.Biz;
using BI = bcp.Biz.Interfaces;
using BS = bcp.Biz.Services;
using BV = bcp.Biz.Validators;
// For 30-x
using DD = bcp.Data;
using DF = bcp.Data.Functions;
using DV = bcp.Data.Views;
using DM = bcp.Data.dbsets;
using CTX = bcp.Data.dbsets.BCP_DBContext;
// For 40-x
using CC = bcp.Common;
using CA = bcp.Common.Constants;
using CAEL = bcp.Common.Constants.ErrorCodeList;
using CDE = bcp.Common.DDEnums;
using CM = bcp.Common.Models;
using CME = bcp.Common.ErrorModels;
using CU = bcp.Common.Utils;

// For 50-x
#endregion // Using Declarations
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security.Principal;


namespace bcp.web.Infra.Filters
{
	public class GateActionFilter : IActionFilter
	{
		internal ILogger<GateActionFilter> log;
		private CC.IAppsettings APPCFG;
		internal CC.UserPayload UPL;
		private CME.IErrorService ERRSVC;
		private BV.CommonBizValidators VCV;
		private bcp.Biz.Validators.AuthzBizValidators VAUTH;
		private BS.AuthzBizService BAS;
		// private bool IsAPI = false;


		public GateActionFilter(ILogger<GateActionFilter> logger, CC.IAppsettings appconfig, CC.UserPayload user_data, CME.IErrorService _es, BV.CommonBizValidators vcv, BS.AuthzBizService sec_svc, bcp.Biz.Validators.AuthzBizValidators v_auth)
		{
			log = logger;
			APPCFG = appconfig;
			UPL = user_data;
			ERRSVC = _es;
			VCV = vcv;
			BAS = sec_svc;
			VAUTH = v_auth;
		}


		public void OnActionExecuting(ActionExecutingContext context)
		{
			// Get Token
			string lsToken = null;
			Guid loTrackID;
			bool IsNewSession = true;

			if (context.HttpContext.User.Identity.IsAuthenticated)
			{
				IsNewSession = false;
				lsToken = context.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.SerialNumber)?.Value ?? string.Empty;
				loTrackID = Guid.Parse(lsToken);
			}
			// else if (context.HttpContext.Request.Headers.ContainsKey(APPCFG.SessionOpts.AuthCookieName))
			// {
			// 	IsNewSession = false;
			// 	lsToken = context.HttpContext.Request.Headers[APPCFG.SessionOpts.AuthCookieName];
			// 	loTrackID = Guid.Parse(lsToken);
			// }
			else
			{
				IsNewSession = true;
				loTrackID = Guid.NewGuid();
				lsToken = loTrackID.ToString("N");
			}
			Trace.CorrelationManager.ActivityId = loTrackID;

			// Get Payload
			CC.UserPayload loUPL = AuthenticateSession(lsToken, IsNewSession, context).Result;
			if (loUPL == null)
			{
				context.Result = new UnauthorizedResult();
				if (context.HttpContext.User.Identity.IsAuthenticated)
				{
					log.LogWarning("TBD. Turn off authentication for the user. Session is compromised.");
					// signout the user
				}
			}
			if (!UPL.Equals(loUPL)) { log.LogError("Payload Mismatch, Review code."); }
			log.LogInformation("Token = {0}, User = {1}, Route = {2}", loUPL?.Token, loUPL?.Login, context.ActionDescriptor.DisplayName);
		}

		public void OnActionExecuted(ActionExecutedContext context)
		{
			log.LogInformation(
				  "Status Code = {0}, Route = {1}, Token = {2}, User = {2}"
				, context.HttpContext.Response.StatusCode
				, context.ActionDescriptor.DisplayName
				, UPL?.Token
				, UPL?.Login
				);
			if (ERRSVC.ErrorCount > 0)
			{
				log.LogError(ERRSVC.GetAllErrorsAsJSON(false));
				log.LogError("Forcing change of status and flushing errors");
				ObjectResult loResult = new ObjectResult(ERRSVC.GetAllErrors(true));
				// TODO: Inject into viewbag for consumption by UI
				// loResult.StatusCode = 500;
				// context.Result = loResult;
			}
		}

		private async Task<CC.UserPayload> AuthenticateSession(string token, bool IsNew, ActionExecutingContext context)
		{
			DM.MaintSessions loSM = null;
			CME.IValidationErrors loValErrs = null;
			DateTime ldtNow = DateTime.UtcNow;

			if (IsNew) { loSM = MakeGuestSession(token); }
			else { (loValErrs, loSM) = await VAUTH.ValidateSession(token, loSM); }

			if (loValErrs?.HasErrors ?? false || loSM == null)
			{
				log.LogWarning("Failing Authentication: Has Errors or Null session returned for existing session lookup.");
				return context.ActionDescriptor.EndpointMetadata.OfType<AllowAnonymousAttribute>().Any()
				? MakeGuestSession(token)?.User_Payload
				: null
				;
			}

			// Allow anyone for anonymous endpoints
			if (
				   !context.ActionDescriptor.EndpointMetadata.OfType<AllowAnonymousAttribute>().Any()
				&& loSM.UserType == (int)CDE.USER_TYPE.ANONYMOUS
				)
			{
				log.LogWarning("Anonymous access attempted to resource [{0}]. Refusing access. Payload = {1}", context.ActionDescriptor.DisplayName, loSM.PayloadData);
				return null;
			}
			if (loSM.UserType == (int)CDE.USER_TYPE.ANONYMOUS)
			{
				log.LogInformation("Allowing anonymous access to public action");
			}


			// TODO: Add logic to expire session if exceeding lifetime.
			// Inspect session token status
			switch ((CDE.SESSION_STATUS)loSM.Status)
			{
				case CDE.SESSION_STATUS.ACTIVE:
					// Allow only valid sessions.
					break;
				case CDE.SESSION_STATUS.EXPIRED:
				case CDE.SESSION_STATUS.LOG_OUT:
				case CDE.SESSION_STATUS.CLOSED:
					log.LogWarning("Token status = {0}. Refusing access for stale token. Possible forced entry. Payload = {1}", loSM.Status, loSM.PayloadData);
					break;
				case CDE.SESSION_STATUS.HOT_LISTED:
				case CDE.SESSION_STATUS.OTHER:
				case CDE.SESSION_STATUS.UNKNOWN:
				default:
					log.LogWarning("Token status = {0}. Refusing access. Possible forced entry. Payload = {1}", loSM.Status, loSM.PayloadData);
					return null;
					// break;
			}

			// 
			// Only authorized users past this point
			// 

			// Update session logs with access timestamp.
			if (!IsNew)
			{
				// update existing instance, or DI references will break
				var loUPL = loSM.User_Payload;
				if (loUPL.Msid != loSM.Msid)
				{
					log.LogWarning("MSID Mismatch! MS = {0}, PL = {1}", loSM.Msid, loUPL.Msid);
				}
				UPL.Msid = loUPL.Msid; // TODO: Check loSM.Msid
				UPL.Token = loUPL.Token;
				UPL.Userid = loUPL.Userid;
				UPL.Login = loUPL.Login;
				UPL.DisplayName = loUPL.DisplayName;
				UPL.Role = loUPL.Role;
				UPL.Type = loUPL.Type;
				UPL.IssueTime = loUPL.IssueTime;
				UPL.AuthTime = loUPL.AuthTime;
				UPL.Status = loUPL.Status;
				UPL.LastUsed = loSM.User_Payload.LastUsed = loSM.LastUsed = ldtNow;
				await BAS.UpdateSessionLog(false);
			}
			// All is well, process to next.
			// return loSM?.Payload;
			return UPL;
		}

		private DM.MaintSessions MakeGuestSession(string token)
		{
			DM.MaintSessions loSM;
			DateTime ldtNow = DateTime.UtcNow;
			string lsID = Guid.NewGuid().ToString("N");

			// update existing instance, or DI references will break
			UPL.Token = token;
			UPL.DeviceSig = "NOT YET IMPLEMENTED";
			UPL.Userid = APPCFG.SessionOpts.ANON_USER_ID;
			UPL.Login = APPCFG.SessionOpts.ANON_LOGIN;
			UPL.DisplayName = APPCFG.SessionOpts.ANON_USER_NAME;
			UPL.Role = CDE.USER_ROLE.UNKNOWN;
			UPL.Type = CDE.USER_TYPE.ANONYMOUS;
			UPL.Status = CDE.SESSION_STATUS.ACTIVE;
			UPL.AuthTime = DateTimeOffset.MinValue;
			UPL.IssueTime = ldtNow;
			UPL.LastUsed = ldtNow;

			int liRes = BAS.UpdateSessionLog(false).Result;
			if (liRes == 0)
			{
				// Failed to create session
				log.LogWarning("Session records update count = 0, expect 3");
				loSM = null;
			}
			else
			{
				// loSM = BAS.GetCurrentSession().Result;
				loSM = new DM.MaintSessions(UPL);
			}

			// TODO: Create session Identity, to track session

			return loSM;
		}

		/**/

	}
}
