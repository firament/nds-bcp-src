namespace bcp.Consts
{
	public static class KEYS
	{
		public const string ERROR_VIEWBAG = "ERROR_MESG_KEY";
		public const string USERID_KEY = "USERID";
		public const string USER_TYPE_KEY = "USERTYPE";
		public const int SYSTEM_USER_ID_VAL = int.MaxValue - 9; // => 2147483638 => 2,147,483,638

	}
	/// <summary>
	/// Move these to App Config.
	/// </summary>
	public static class FORMATS
	{
		// MOVE FOLLOWING TO APP CONFIG
		public const string DATE_FORMAT = "dd MMM yyyy [ddd]";
		public const string DATE_SHORT_FORMAT = "dd-MM-yyyy";
		public const string DATE_TIME_FORMAT = "dd MMM yyyy HH:mm tt";
	}
}
